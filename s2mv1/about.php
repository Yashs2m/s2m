<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Stones2Milestones is an organisation in the education space that aims to impact the way in which school going children between the ages of 3 to class 3 learn to read in English.">
    <meta name="author" content="">
    <meta name="keywords" content="Reading,children, grade 1 English, grade 2 English, English, child,Novel, learn, learn English, learn reading,stones, stones2milestones, reader, English grammar, language, english Novel, blog , about , about s2m, s2m , wingsofwords, getfreadom, wowconnect , wow">
    <title>Stones2Milestones| About Us</title>
    <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="img/s2sfavicon.png"/>
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/main.css">   
    <link rel="stylesheet" href="css/modal.css">   
    <link href="css/new-age.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/owl.theme.default.min.css" rel="stylesheet">
    <link href="css/footer.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/YouTubePopUp.css">
    <script src="https://cdn.firebase.com/js/client/2.2.0/firebase.js"></script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-81416339-1', 'auto');
      ga('send', 'pageview');

    </script> 
</head>
<style type="text/css">
.typeform-popup-wrapper.typeform-popup-mode-popup{
        width: 50%!important;
    height: 80%!important;
    top: 10%!important;
    left: 25%!important;
      }
#stickyHeading.stickHeading{
  padding-top: 15%;
}
#stickyHeading1.stickHeading{
  padding-top: 15%;
}
#stickyHeading2.stickHeading{
  padding-top: 15%;
}
#stickyHeading3.stickHeading{
  padding-top: 15%;
}
.stickli{
  color: green;
  border-radius: 0px;
  border-bottom: 2px solid green;
}
#sticky.stick {
    width: 100%;
    left: 0%;
    background: white;
    text-align: center;
    position: fixed;
    top: 70px;
    z-index: 2;
}
@media only screen and (max-width: 768px) {
        #sticky{
          display: none;
        }
}

.color-change{
    border: 1px solid #39a935;
  }
  .color-change-p{
    color: #39a935;
  }
</style>
<style type="text/css">
      .header-overlay{
        background: rgba(0,0,0,0.9)
      }
</style>
<style type="text/css">
  .flip-container {
    -webkit-perspective: 1000;
    -moz-perspective: 1000;
    -o-perspective: 1000;
    perspective: 1000;
    }

    .flip-container:hover .flipper,  
    .flip-container.hover .flipper {
      -webkit-transform: rotateY(180deg);
      -moz-transform: rotateY(180deg);
      -o-transform: rotateY(180deg);
      transform: rotateY(180deg);
    }

  .flip-container, .front, .back {
    width: 350px;
    height: 250px;
  }

  .flipper {
    -webkit-transition: 0.6s;
    -webkit-transform-style: preserve-3d;

    -moz-transition: 0.6s;
    -moz-transform-style: preserve-3d;
    
    -o-transition: 0.6s;
    -o-transform-style: preserve-3d;

    transition: 0.6s;
    transform-style: preserve-3d;

    position: relative;
  }

  .front, .back {
    -webkit-backface-visibility: hidden;
    -moz-backface-visibility: hidden;
    -o-backface-visibility: hidden;
    backface-visibility: hidden;

    position: absolute;
    top: 0;
    left: 0;
  }

  .front1 {
    background: url(img/core01.png) 0 0 no-repeat;
    z-index: 2;
  }

  .front2 {
    background: url(img/core02.png) 0 0 no-repeat;
    z-index: 2;
  }

  .front3 {
    background: url(img/core03.png) 0 0 no-repeat;
    z-index: 2;
  }

  .front4 {
    background: url(img/core04.png) 0 0 no-repeat;
    z-index: 2;
  }

  .front5 {
    background: url(img/core05.png) 0 0 no-repeat;
    z-index: 2;
  }

  .front6 {
    background: url(img/core07.png) 0 0 no-repeat;
    z-index: 2;
  }

  .back {
    -webkit-transform: rotateY(180deg);
    -moz-transform: rotateY(180deg);
    -o-transform: rotateY(180deg);
    transform: rotateY(180deg);

    background: #f8f8f8;
  }
  /*
  .front .name {
    font-size: 2em;
    display: inline-block;
    background: rgba(33, 33, 33, 0.9);
    color: #f8f8f8;
    font-family: Courier;
    padding: 5px 10px;
    border-radius: 5px;
    bottom: 60px;
    left: 25%;
    position: absolute;
    text-shadow: 0.1em 0.1em 0.05em #333;

    -webkit-transform: rotate(-20deg);
    -moz-transform: rotate(-20deg);
    -o-transform: rotate(-20deg);
    transform: rotate(-20deg);
  }*/

  .back p {
    position: absolute;
      left: 0;
      font-size: 14px;
      right: 0;
      text-align: center;
      padding: 0px 20px;
      line-height: 24px;
      color: black;

  }
  .front .name{
    margin-top: 230px;
  }
  .front .name p{
    margin-bottom: 0px;
    color: black;
  }
</style>
<style type="text/css">
    /*Some CSS*/
.img img{
  padding-bottom: 10px;
  width: 200px;
}
.img span{
  color: black;
  font-size: 20px;
}
.core-teamdiv{
  padding-top: 40px;
  padding-bottom: 40px;
}
.nav-pills > li > a{
    border-radius: 0px;
  }
  .navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:hover, .navbar-default .navbar-nav > .open > a:focus{
    background: none!important;
    color: white!important;
  }
@media only screen and (max-width: 768px){
  .img img{
    width: 200px!important;
  }
} 
.team-name p{
  color: black;
    font-size: 16px;
    font-weight: bolder;
}
#visions2m{
  padding-top: 30px;
}
#missions2m{
  padding-top: 60px;
}
#corevaluess2m{
  padding-top: 30px;
}
.mobile-view-pills{
      border-right: none;
    width: 24%;
    float: left;
    font-size: 12px;
    font-weight: 600;
    font-family: inherit;
    text-transform: uppercase;
}
.mobile-view-pills:hover{
      border-right: none;
      border-bottom: 2px solid green;
    width: 24%;
    float: left;
    
    font-weight: 600;
    font-family: inherit;
    text-transform: uppercase;
}
.nav-pills-color{
  color: black;
}
.nav-pills-color:hover{
  color: green;
}
</style>        

<body id="page-top" data-spy="scroll" data-target=".nav-pills" data-offset="50">
<!-- Nav Bar starts -->
     <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span><i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand " href="http://www.stones2milestones.com/">
                    <img src="img/s2s.png" style="width: 150px;">
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                      <li class="dropdown list-view">
                        <a href="http://www.stones2milestones.com/about.php" class="dropdown-toggle page-scroll" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <p>about</p></a>
                        <ul class="dropdown-menu">
                          <li><a href="http://www.stones2milestones.com/about.php">About</a></li>
                          <li><a href="http://www.stones2milestones.com/about.php#visions2m">Vision</a></li>
                          <li><a href="http://www.stones2milestones.com/about.php#missions2m">Mission</a></li>
                          <li><a href="http://www.stones2milestones.com/about.php#corevaluess2m">Core Values</a></li>
                          <li><a href="http://www.stones2milestones.com/about.php#teams2m">Team</a></li>
                        </ul>
                      </li>
                      <li class="dropdown list-view">
                        <a href="#" class="dropdown-toggle page-scroll" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <p>For Schools</p></a>
                        <ul class="dropdown-menu">
                          <li><a href="http://www.wingsofwords.com/">Wings of Words</a></li>
                          <li><a href="https://play.google.com/store/apps/details?id=com.wowconnect">WOWConnect</a></li>
                          <li><a href="http://www.wingsofwords.com/blog">Blog</a></li>
                        </ul>
                      </li>
                      <li class="dropdown list-view">
                        <a href="#" class="dropdown-toggle page-scroll" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <p>For Parents</p></a>
                        <ul class="dropdown-menu">
                          <li><a href="https://www.getfreadom.com/">Freadom</a></li>
                          <li><a href="https://play.google.com/store/apps/details?id=com.application.freadom&hl=en">Download App</a></li>
                          <li><a href="https://getfreadom.com/blog/">Blog</a></li>
                        </ul>
                      </li>
                      <li class="list-view mobile-view">
                        <a class="page-scroll" href="http://www.stones2milestones.com/about.php">
                            <p>About</p>
                        </a>
                      </li>
                      <li class="list-view mobile-view">
                        <a class="page-scroll" href="http://www.stones2milestones.com/partners.php">
                            <p>Partner With Us</p>
                        </a>
                      </li><li class="list-view mobile-view">
                        <a class="page-scroll" href="http://www.wingsofwords.com/">
                            <p>Wings of words</p>
                        </a>
                      </li><li class="list-view mobile-view"> 
                        <a class="page-scroll" href="https://play.google.com/store/apps/details?id=com.wowconnect">
                            <p>WOWConnect</p>
                        </a>
                      </li><li class="list-view mobile-view">
                        <a class="page-scroll" href="https://www.getfreadom.com/">
                            <p>Freadom</p>
                        </a>
                      </li><li class="list-view mobile-view" >
                        <a class="page-scroll" href="https://play.google.com/store/apps/details?id=com.application.freadom&hl=en">
                            <p>Download the App Freadom</p>
                        </a>
                      </li>
                      <li class="list-view">
                        <a class="page-scroll" href="http://www.stones2milestones.com/blog/">
                            <p>Blog</p>
                        </a>
                      </li>
                     <li class="dropdown list-view">
                        <a href="http://www.stones2milestones.com/partners.php" class="dropdown-toggle page-scroll" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <p>Partner With Us</p></a>
                        <ul class="dropdown-menu">
                          <li><a href="http://www.stones2milestones.com//partners.php">Partners</a></li>
                          <li><a href="http://www.stones2milestones.com//partners.php#engages2m">How Do We Enagage?</a></li>
                          <li><a href="http://www.stones2milestones.com//partners.php#partnerss2m">Partner Schools</a></li>
                          <li><a href="http://www.stones2milestones.com//partners.php#csrs2m">Socail Impact</a></li>
                          <li><a href="http://www.stones2milestones.com//partners.php#testimonials2m">Testimonial</a></li>
                        </ul>
                      </li>
                      <li class="exp-us list-view">
                        <a class="typeform-share button" href="https://stones2milestones.typeform.com/to/ksK99C" data-mode="popup" style="padding: 6px;" target="_blank"><p class="p-tag-exp" style="margin-bottom: 0px;
    text-transform: uppercase;    font-size: 10px;">Join the mission</p></a>
                    </li>
                    <!-- <li>
                      <button type="button" class="btn btn-info btn-lg" >Open Modal</button>
                    </li> -->
                    <!-- Modal -->

                        
                    <li class="phone">
                        <span id="phone-logo"><img src="img/phonecall.svg" style="margin-right:5px; width: 20px;">+91 90770 77777</span>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
<div class="modal fade" id="myModal" role="dialog">
                          <div class="modal-dialog">
                          
                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">×</button>
                                <h4 class="modal-title">Join The Mission</h4>
                              </div>
                              <div class="modal-body">
                              <div role="form" class="wpcf7" id="wpcf7-f433-o1" lang="en-US" dir="ltr">
                      <div class="screen-reader-response"></div>
                      <form action="" method="post" id= "newActivity" name="newActivity" class="" novalidate="novalidate">
                      <div style="display: none;">
                      <input type="hidden" name="_wpcf7" value="433">
                      <input type="hidden" name="_wpcf7_version" value="4.5.1">
                      <input type="hidden" name="_wpcf7_locale" value="en_US">
                      <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f433-o1">
                      <input type="hidden" name="_wpnonce" value="37e90b15f0">
                      </div>
                      <p><span class="wpcf7-form-control-wrap iam"><select id="selectParentTeacher" name="iam" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required pop-in" aria-required="true" aria-invalid="false"><option value="I am a Parent">I am a Parent</option><option value="I am a Teacher">I am a Teacher</option><option value="I am a School Admin">I am a School Admin</option></select></span><br>
                      <span class="wpcf7-form-control-wrap name">
                      <input type="text" id="formName" name="name" required  value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required pop-in" aria-required="true" aria-invalid="false" placeholder="Your name"></span><br>
                      <span class="wpcf7-form-control-wrap phone">
                      <input type="tel" name="phone" id="formPhone" required value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel pop-in" aria-required="true" aria-invalid="false" placeholder="Mobile number"></span><br>
                      <span class="wpcf7-form-control-wrap email">
                      <input type="email" id="formEmail" name="email" value="" required size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email pop-in" aria-required="true" aria-invalid="false" placeholder="Email address"></span><br>
                      <span class="wpcf7-form-control-wrap school">
                      <input type="text" id="formText"name="school" value="" size="40" class="wpcf7-form-control wpcf7-text pop-in" aria-invalid="false" placeholder="School name (if you are a Teacher or School Admin)"></span><br>
                      <input type="submit" id="saveFrom" value="Request" class="wpcf7-form-control wpcf7-submit popbtn addValue"><img class="ajax-loader" src="" alt="Sending ..." style="visibility: hidden;"></p>
                      <div class="wpcf7-response-output wpcf7-display-none"></div></form></div>        </div>
                            </div>
                            
                          </div>
</div>

    <header class="header-about">
      <div class="header-overlay">
          <div class="container" style="">
          <div class="header-about-tag">
            Helping each child find <br>
            their place in the world 
          </div>
         </div>    
      </div>
    </header>
    <section id="home" class="home bg-primary text-center parallax">
        <div class="container text-center mobile-bg-about">
          <div class="header-overlay">
            <div class="col-xs-12 ">
              <div class="col-xs-12">
                <h1 class="heading-tag-mobile">Helping each child find their place in the world</h1>
              </div>
              <div class="col-xs-12 viedo-bg" >
                <!-- <a href="https://www.youtube.com/watch?v=ygwRc9WuBBM" class="demo" data-toggle="tooltip" title="" data-original-title="Personal Message From The Founder!">
                    <img src="img/playsvgmedia.svg" style="    width: 40px;padding-bottom: 20px;"> -->
              </div>
            </div>
          </div>          
        </div>
        <div id="sticky-anchor">          
        </div>
        <div id="sticky">
            <div class="container" >
          <ul class="nav nav-pills" >
            <li class="mobile-view-pills" ><a id="stickyli1" class="nav-pills-color" href="#visions2m">Who We Are?</a></li>
            <li class="mobile-view-pills"><a id="stickyli2" class="nav-pills-color" href="#missions2m">Mission</a></li>
            <li class="mobile-view-pills"><a id="stickyli3" class="nav-pills-color" href="#corevaluess2m">Core Values</a></li>
            <li class="mobile-view-pills"><a id="stickyli4" class="nav-pills-color" href="#teams2m">Team</a></li>
            <!-- <li><a class="nav-pills-color" href="#teams2m">Advisory Board</a></li> -->
          </ul>
        </div>
        </div>
        
        <div class="container" id="visions2m" style="">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class=" heading " id="stickyHeading">
                    <h1 class="heading-desc">
                        Who We Are?
                    </h1>                
                </div>
                <div class="wishtoachieve">
                  <div class="col-lg-12 col-md-12 col-sm-12">
                    <img src="img/Our-Journey.png">
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 content-div">
                    <p>
                      We envision a world where children are happy, productive and free;<br>
                      Happy in who they are;<br>
                      Productive in shaping their families and communities;<br>
                      Free to explore the world, curious and uninhibited.<br>
                      Let’s build this world together.<br>
                      Brick by brick.
                    </p>
                  </div>
                </div>
            </div>         
        </div>  
        <div class="container text-center" id="missions2m">
            <div>
                 <div class="col-sm-12 col-md-12 col-lg-12 heading" id="stickyHeading1">
                     <h1>
                        What We Wish To Achieve
                     </h1>
                 </div> 
                 <div class="col-sm-12 col-md-12 col-lg-12 sub-heading">
                     <h3>Stones2milestones is on a mission to ‘Create a Nation of Readers’.</h3>
                 </div>
                 <div class="col-sm-12 col-md-12 col-lg-12 content-div">
                     <p>Since English is a major international language, the ability to read fluently in English is a life­ skill that is crucial to academic success and to participate in the global arena. However, for several reasons, Indian children are trailing behind in their ability to read and comprehend English and we can no longer ignore the research. A child who doesn’t read at grade level expectancy by class 4 will never catch up!<br><br>
                     It is our goal to change this even for children in non English environments. We want to make learning to read a conscious design of curriculum. The focus is on enabling the skill to make reading easy and also on developing the will to make reading enjoyable. As the window for this intervention is from age 3 to age 9, we integrate this program with the early school curriculum. Besides providing this to schools, we also equip the parents to support this growth in their children.</p>
                 </div>
            </div>  
        </div>
        <div class="container" id="corevaluess2m" style="">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class=" heading " id="stickyHeading2">
                      <h1 class="heading-desc">
                          Core Values
                      </h1>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-12">                 
                  <div class="col-sm-4 col-md-4 col-lg-4 core-values" >
                    <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                      <div class="flipper">
                        <div class="front front1">
                         <div class="name"> <span><p>We are One</p></span></div>
                        </div>
                        <div class="back">                          
                          <p>
                            <br>
                            Each person connected to S2M – funder, employee, customer, and well wisher is a member of our large family. We extend unconditional acceptance to all members of our family. We share joys and sorrows and support each other. We have compassionate conversations honestly sharing our feelings, needs and deep truth and this is our highest offering of love.
                          </p>
                        </div>
                      </div>
                    </div>
                    </div>                  
                    <div class="col-sm-4 col-md-4 col-lg-4 core-values">
                      <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                        <div class="flipper">
                          <div class="front front2">
                           <div class="name"> <span><p>We Choose What We Love</p></span></div>
                          </div>
                          <div class="back">
                            
                            <p><br><br>We intentionally welcome people to define their roles and contribution, which comes from their best. We engage in open conversations to continuously ensure each person feels joyful about what they do. We transmit this joy of working into the larger world so that more and more people want to be a part of our family.               </p>
                          </div>
                        </div>
                      </div>
                    </div>                    
                    <div class="col-sm-4 col-md-4 col-lg-4 core-values">
                      <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                        <div class="flipper">
                          <div class="front front3">
                           <div class="name"> <span><p>We Live Up To Our Promise</p></span></div>
                          </div>
                          <div class="back">
                            <br><br><br><br>
                            <p>We always aspire to live up to our highest values. We are transparent and upfront in our processes. Our customers and partners can rely on us to be fair, responsible, honest and deliver what we promise.               </p>
                          </div>
                        </div>
                      </div>
                    </div>        
                </div>
                <div class="col-sm-12 col-md-12 col-lg-12" style="margin-top: 15px;">
                  
                    <div class="col-sm-4 col-md-4 col-lg-4 core-values">
                      <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                        <div class="flipper">
                          <div class="front front4">
                           <div class="name"> <span><p>We Pursue Quality In Everything</p></span></div>
                          </div>
                          <div class="back">
                            <br><br><br>
                            <p>We delight in exceeding all expectations and have our eye on all that it takes to get there. Our team works as one force to monitor, inquire, respond, learn correct course and innovate so that we reach high standards of delivery in all the aspects of our mission.                </p>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <div class="col-sm-4 col-md-4 col-lg-4 core-values">
                      <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                        <div class="flipper">
                          <div class="front front5">
                           <div class="name"> <span><p>We Lay Foundations</p></span></div>
                          </div>
                          <div class="back">
                            <br><br><br>
                            <p>The mission of S2M is an eternal idea that belongs to the universe.  Each member of S2M pays a role in service to that mission which is greater than the individual. We are custodians who lay foundations for future members to build upon.               </p>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <div class="col-sm-4 col-md-4 col-lg-4 core-values">
                      <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                        <div class="flipper">
                          <div class="front front6">
                           <div class="name"> <span><p>We Make It Happen</p></span></div>
                          </div>
                          <div class="back">
                            <br><br><br>
                            <p>We face fair weather and storms with our focus on our goal. Nothing deters us from our mission. This steadfastness helps us to solve all problems, overcome obstacles and find ever new ways of doing our job well.</p>
                          </div>
                        </div>
                      </div>
                    </div>                    
                </div>
            </div>
        </div>        
        <div class="container" id="teams2m" style="">
            <div class="col-lg-12">
                <div class=" heading " id="stickyHeading3">
                    <h1 class="heading-desc">
                      We Have A Team That Creates Magic
                    </h1>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="col-md-3 col-lg-3 col-sm-12 core-team">
                    <div class="img row">
                      <img src="img/team01.png" title="Kavish Gadia" alt="Kavish Gadia">
                      
                    </div>
                    <div class="row team-name">
                      <p>Kavish Gadia</p>
                    </div>
                  </div>
                  <div class="col-md-3 col-lg-3 col-sm-12 core-team">
                    <div class="img">
                      <img src="img/team04.png" title="Nikhil Saraf" alt="Nikhil Saraf">
                      
                    </div>
                    <div class="row team-name">
                      <p>Nikhil Saraf</p>
                    </div>
                  </div>
                  <div class="col-md-3 col-lg-3 col-sm-12 core-team">
                    <div class="img">
                      <img src="img/team02.png" title="Jagruti Gala" alt="Jagruti Gala">
                      
                    </div>
                    <div class="row team-name">
                      <p>Jagruti Gala</p>
                    </div>
                  </div>
                  <div class="col-md-3 col-lg-3 col-sm-12 core-team">
                    <div class="img">
                      <img src="img/team03.png" title="Aditi Mehta" alt="Aditi Mehta">

                    </div>
                    <div class="row team-name">
                      <p>Aditi Mehta</p>
                    </div>
                  </div>
                  
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 core-teamdiv" style="  ">
                  <div class="col-md-3 col-lg-3  col-sm-12 core-team">
                    <div class="img">
                      <img src="img/team05.png" title="Raseel Arunkant" alt="Raseel Arunkant">
                     
                    </div>
                    <div class="row team-name">
                      <p>Raseel Arunkant</p>
                    </div>
                  </div>
                  <div class="col-md-3 col-lg-3 col-sm-12 core-team">
                    <div class="img">
                      <img src="img/Amit Agarwala.png" title="Amit Agarwala" alt="Amit Agarwala">
                      
                    </div>
                    <div class="row team-name">
                      <p>Amit Agarwala</p>
                    </div>
                  </div>
                  <div class="col-md-3 col-lg-3 col-sm-12 core-team" >
                    <div class="img">
                      <img src="img/team11.png" title="Bidhan Sarkar" alt="Bidhan Sarkar">
                      
                    </div>
                    <div class="row team-name">
                      <p>Bidhan Sarkar</p>
                    </div>
                  </div>                  
                  <div class="col-md-3 col-lg-3 col-sm-12 core-team">
                    <div class="img">
                      <img src="img/team07.png" title="Nisha Ghai" alt="Nisha Ghai">
                      
                    </div>
                    <div class="row team-name">
                      <p>Nisha Ghai</p>
                    </div>
                  </div>                  
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 core-teamdiv" >
                  <div class="col-md-3 col-lg-3 col-sm-12 core-team">
                    <div class="img">
                      <img src="img/team08.png" title="Shipra Parikh" alt="Shipra Parikh">
                      
                    </div>
                    <div class="row team-name">
                      <p>Shipra Parikh</p>
                    </div>
                  </div>
                  <div class="col-md-3 col-lg-3 col-sm-12 core-team">
                    <div class="img">
                      <img src="img/team13.png" title="Swati Rohatgi" alt="Swati Rohatgi">
                      
                    </div>
                    <div class="row team-name">
                      <p>Swati Rohatgi</p>
                    </div>
                  </div>
                  <div class="col-md-3 col-lg-3 col-sm-12 core-team">
                    <div class="img">
                      <img src="img/team06.png" title="Ami Gandhi" alt="Ami Gandhi">
                      
                    </div>
                    <div class="row team-name">
                      <p>Ami Gandhi</p>
                    </div>
                  </div>
                  <div class="col-md-3 col-lg-3 col-sm-12 core-team">
                    <div class="img">
                      <img src="img/Anuradha Baraya.png" title="Anuradha Baraya" alt="Anuradha Baraya">
                      
                    </div>
                    <div class="row team-name">
                      <p>Anuradha Baraya</p>
                    </div>
                  </div> 
                  
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 core-teamdiv">
                  <div class="col-md-3 col-lg-3 col-sm-12 core-team">
                    <div class="img">
                      <img src="img/Anuradha Kaistha.png" title="Anuradha Kaistha" alt="Anuradha Kaistha">
                      
                    </div>
                    <div class="row team-name">
                      <p>Anuradha Kaistha</p>
                    </div>
                  </div>
                  <div class="col-md-3 col-lg-3 col-sm-12 core-team">
                    <div class="img">
                      <img src="img/yash.png" title="Yash Gupta" alt="Yash Gupta">
                      
                    </div>
                    <div class="row team-name">
                      <p>Yash Gupta</p>
                    </div>
                  </div>
                   <div class="col-md-3 col-lg-3 col-sm-12 core-team">
                    <div class="img">
                      <img src="img/Menaka Warrier.png" title="Menaka Warrier" alt="Menaka Warrier">
                      
                    </div>
                    <div class="row team-name">
                      <p>Menaka Warrier</p>
                    </div>
                  </div> 
                  <div class="col-md-3 col-lg-3 col-sm-12 core-team">
                    <div class="img">
                      <img src="img/Akash Mohanty.png" title="Akash Mohanty" alt="Akash Mohanty">
                      
                    </div>
                    <div class="row team-name">
                      <p>Akash Mohanty</p>
                    </div>
                  </div>                                                    
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 core-teamdiv">
                  <div class="col-md-3 col-lg-3 col-sm-12 core-team">
                    <div class="img">
                      <img src="img/Kajal Marwari.png" title="Kajal Marwari" alt="Kajal Marwari">
                      
                    </div>
                    <div class="row team-name">
                      <p>Kajal Marwari</p>
                    </div>
                  </div>

                  <div class="col-md-3 col-lg-3 col-sm-12 core-team">
                    <div class="img">
                      <img src="img/Anmol Goyanka.png" title="Anmol Goyanka" alt="Anmol Goyanka">
                      
                    </div>
                    <div class="row team-name">
                      <p>Anmol Goyanka</p>
                    </div>
                  </div>
                  <div class="col-md-3 col-lg-3 col-sm-12 core-team">
                    <div class="img">
                      <img src="img/Prashanr.png" title="Prashant Kakkar" alt="Prashant Kakkar">
                      
                    </div>
                    <div class="row team-name">
                      <p>Prashant Kakkar</p>
                    </div>
                  </div>
                  
                </div>
            </div>
        </div>
        <section class="success fr_sec yellow newsletter_sec">
        <div class="container">
            <div class="row">
                <div class="newsletter_holder col-xs-12">
                    <div class="newsletter_heading col-xs-12">
                        <h4 class="newsletter_title">Sign up to receive amazing stuff around reading</h4>
                        <!-- <h5 class="newsletter_subtitle">Delivered weekly straight into your inbox</h5> -->
                    </div>
                    <div class="newsletter_form col-xs-12 col-xs-offset-0 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                        <form action="http://sendy.stones2milestones.com/subscribe" method="POST" accept-charset="utf-8" class="flex_holder" style=" border: 2px solid #0e6837;box-shadow: 0px -1px 10px -3px;">
                            <!-- <input type="hidden" name="u" value="3eaaed45454f13a1c2c555c5a"> -->
                            <input type="hidden" name="list" value="joNJFfKW2v484XtmxrUEtA"/>
                            <!-- <input type="hidden" name="id" value="cc047a2e71"> -->
                            <div class="flex_elements input_with_icon">
                            <i class="fa fa-paper-plane" aria-hidden="true"></i>
                                <input type="email" autocapitalize="off" autocorrect="off" name="MERGE0" id="MERGE0" size="25" value="" class="inputtext" placeholder="Enter your Email">
                            </div>
                            
                            <a href="#contactModalsign" role="button"  data-toggle="modal">
                            <input type="submit"  name="submit" id="submit" class="flex_elements btn btn-primary submitsignup" value="Sign Up">
                            </a>
                            <div id="contactModalsign" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <!-- <h3 id="myModalLabel">Almost finished...</h3> -->
                                  </div>
                                  <div class="modal-body">
                                   <div class="s2mblack" style="padding: 20px;">
                                       <img src="img/s2mblack.png">
                                   </div>
                                   <div style=" padding: 30px;text-align:center; margin-top: -80px;">
                                       <p>Thank you for subscribing!</p>
                                   </div>
                                  </div>
                                  <!-- <div class="modal-footer" style="border: none;">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true" style="float: left; margin-left: 44%;">Cancel</button>
                                  </div> -->
                                </div>
                              </div>
                            </div>   
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </section>

       <div class="container" style="padding-top: 60px; padding-bottom: 40px; ">
            <!-- Set up your HTML -->
            <div class="owl-carousel owl2-style" id="owl2">
                
                
                <a href="#"><img src="img/youwecan.png" alt="logo05" title="logo05" style="width:150px!important;"></a>
                <a href="#"><img src="img/orchid.png" alt="logo05" title="logo05"></a>
                <a href="#"><img src="img/logo02-2.jpg" alt="logo05" title="logo05"></a>
                <a href="#"><img src="img/logo06-1.jpg" alt="logo05" title="logo05"></a>
                <a href="#"><img src="img/manyavar.png" alt="logo05" title="manyavar" style="width: 150px!important;"></a>
                <a href="#"><img src="img/inclusive-india-logo.png" alt="blackstone-logo" title="blackstone-logo" style="width:150px!important;padding-top: 10px;"></a>
                <a href="#"><img src="img/logo03-1.jpg" alt="logo05" title="logo05" style="width:150px!important;padding-top: 5px;"></a>   
                <a href="#"><img src="img/teach4india.png" alt="logo05" title="logo05" style="width: 180px!important; padding-top: 30px;"></a>                        
            </div>
        </div>

    <section class="school-count">
         <div class="container">
            <div class="col-md-12 col-sm-12 col-lg-12 ">
                <div class="col-xs-4 col-md-4 col-lg-4 school-count-div">
                <div class="col-xs-6 col-md-6 col-lg-6">
                    <img src="img/01png.png" class="school-count-width">
                </div>
                <div class="col-md-6 col-lg-6 col-xs-6 school-count-tag">
                <div class="row count">
                    120
                </div>
                <div class="row">
                    <h3 class="school-tag">Schools</h3>
                </div>
                </div>
                </div>
                <div class="col-xs-4 col-md-4 col-lg-4 school-count-div">
                <div class="col-xs-6 col-md-6 col-lg-6">
                    <img src="img/02png.png" class="school-count-width">
                </div>
                <div class="col-md-6 col-lg-6 col-xs-6 school-count-tag">
                <div class="row count">
                700
                </div>
                <div class="row">
                    <h3 class="school-tag">Teachers</h3>
                </div>
                </div>
                </div>
                <div class="col-xs-4 col-md-4 col-lg-4 school-count-div">
                <div class="col-xs-6 col-md-6 col-lg-6">
                    <img src="img/03png.png" class="school-count-width">
                </div>
                <div class="col-md-6 col-lg-6 col-xs-6 school-count-tag">
                <div class="row count">
                   30000
                </div>
                <div class="row">
                    <h3 class="school-tag">Students</h3>
                </div>
                </div>
                </div>
            </div>
        </div>
    </section>
   <footer>
        <div class="footer-above">
            <div class="container">
                <div class="col-xs-12 col-md-4 col-lg-4">
                    <a href="http://www.stones2milestones.com/"><img class="img-responsive" src="img/s2s.png" style="width: 200px;padding-bottom: 10px;"></a>
                    <p class="footer-p-tag">
                        Creating a nation of 10 million readers by 2022                        
                    </p>
                     <p class="footer-p-tag">                        
                        We are an organisation in the education space <br> that aims to impact the way in which children <br> learn to read in English.
                    </p>
                    <div class="col-lg-12 col-md-12 col-xs-12" style="padding-left: 0px;">
                    <ul class="socialMediaIcons">
                        <li class="facebookIcon socialLinks">
                            <a href="https://www.facebook.com/stones2milestones/">
                                <div class="fb"></div>
                            </a>
                        </li>
                        <li class="linkedIn socialLinks">
                            <a href="https://www.linkedin.com/company-beta/1974028/">
                                <div class="linkedin"></div>
                            </a>
                        </li>
                        <li class="blog socialLinks">
                            <a href="http://stones2milestones.com/blog">
                                <div class="blog"></div>
                            </a>
                        </li>
                        <li class="twitter socialLinks">
                            <a href="https://twitter.com/S2MTweets">
                                <div class="twitter"></div>
                            </a>
                        </li>
                        <!-- <li class="google socialLinks">
                            <a href="#">
                                <div class="googleplus"></div>
                            </a>
                        </li>  -->
                    </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-md-2 col-lg-2 ">
                    
                   
                        <p class="footer-p-tag-head">Company</p>
                        <p class="footer-p-tag"><a href="http://www.stones2milestones.com/about.php">About</a>  </p>
                        <p class="footer-p-tag"><a href="mailto:work@stones2milestones.com" > Careers</a> </p>
                        <p class="footer-p-tag"><a href="http://www.stones2milestones.com/partners.php">Partner With Us</a></p>
                        <!-- <p class="footer-p-tag"><a href="mailto:work@stones2milestones.com" >Contact Us</a></p> -->
                        <p class="footer-p-tag"><a href="http://www.stones2milestones.com/blog">Blog</a></p>
                        <p class="footer-p-tag"><a href="http://www.stones2milestones.com/policy.php">Policy</a></p>
                   
                </div>
                <div class="col-xs-12 col-md-2 col-lg-2 ">
                   
                        <p class="footer-p-tag-head">For Schools</p>
                        <p class="footer-p-tag"><a href="http://www.wingsofwords.com/">Wings of Words</a></p>
                        <p class="footer-p-tag"><a href="https://play.google.com/store/apps/details?id=com.wowconnect">WOW Connect</a></p>
                        <p class="footer-p-tag"><a href="http://www.wingsofwords.com/blog">Blog</a></p>
                        <p class="footer-p-tag"><a href="https://play.google.com/store/apps/details?id=com.wowconnect"><img src="img/googleplay.png" class="footer-google"></a></p>

                        
                   
                </div>
                <div class="col-xs-12 col-md-2 col-lg-2 ">
                   
                        <p class="footer-p-tag-head">For Parents</p>
                        <p class="footer-p-tag"><a href="http://www.getfreadom.com/">Freadom</a></p>
                        <p class="footer-p-tag"><a href="https://play.google.com/store/apps/details?id=com.application.freadom&hl=en">Download App</a></p>
                        <p class="footer-p-tag"><a href="http://www.getfreadom.com/blog">Blog</a></p>
                        <p class="footer-p-tag"><a href="https://play.google.com/store/apps/details?id=com.application.freadom&hl=en"><img src="img/googleplay.png" class="footer-google"></a></p>
                        
                   
                    <!-- <div class="col-xs-12">
                        <p class="footer-p-tag-head">CONTACT</p>
                        <p class="footer-p-tag">support@stones2milestones.com</p>
                        <p class="footer-p-tag ">Gurgaon </p>
                        <p class="footer-p-tag">419, Tower A, Spaze ITech Park, Sohna Road, Sector 49, Gurgaon - 122018, Haryana</p>
                        <p class="footer-p-tag">Phone: +91 40 4137 1111 </p>
                    </div> -->
                </div>
                <div class="col-xs-12 col-md-2 col-lg-2 ">
                   
                        <p class="footer-p-tag-head">Location</p>
                        <p class="footer-p-tag ">
                         Gurgaon:
                      </p>
                     <p class="footer-p-tag">419, Tower A, Spaze ITech Park, Sohna Road, <br>Sector 49, Gurgaon - 122018, Haryana
                     </p>
                     <!-- <p class="footer-p-tag"></p> -->
                     <p class="footer-p-tag">Phone: +91 90770 77777</p>      
                   
                </div>
            </div>
        </div>
        <!-- <div class="footer-below">
            <div class="container">
                <div class="col-md-12 col-xs-12 col-lg-12">
                    <p class="footer-p-tag-head footer-p-tag-font">Locations</p>
                </div>
                 <div class="col-xs-12 col-md-4">
                     <p class="footer-p-tag footer-p-tag-font">
                         Gurgaon:
                     </p>
                     <p class="footer-p-tag">419, Tower A, Spaze ITech Park, Sohna Road, 
                     </p>
                     <p class="footer-p-tag">Sector 49, Gurgaon - 122018, Haryana</p>
                     <p class="footer-p-tag">Phone: 09742087612</p>                     
                 </div>

                 <div class="col-xs-12 col-md-4">
                     <p class="footer-p-tag footer-p-tag-font">
                         Bengaluru:
                     </p>
                     <p class="footer-p-tag">4th Floor, Salarpuria Towers,No 22, Hosur Main road ,
                     </p>
                     <p class="footer-p-tag">6th Block, Koramangala, Bengaluru - 560034</p>
                     <p class="footer-p-tag">Phone: 09742087612</p>                     
                 </div>

                 <div class="col-xs-12 col-md-4">
                     <p class="footer-p-tag footer-p-tag-font">
                         Bengaluru:
                     </p>
                     <p class="footer-p-tag">4th Floor, Salarpuria Towers,No 22, Hosur Main road ,
                     </p>
                     <p class="footer-p-tag">6th Block, Koramangala, Bengaluru - 560034</p>
                     <p class="footer-p-tag">Phone: 09742087612</p>                     
                 </div>

                 <div class="col-xs-12 col-md-4">
                     <p class="footer-p-tag footer-p-tag-font">
                         Bengaluru:
                     </p>
                     <p class="footer-p-tag">4th Floor, Salarpuria Towers,No 22, Hosur Main road ,
                     </p>
                     <p class="footer-p-tag">6th Block, Koramangala, Bengaluru - 560034</p>
                     <p class="footer-p-tag">Phone: 09742087612</p>                     
                 </div>

                 <div class="col-xs-12 col-md-4">
                     <p class="footer-p-tag footer-p-tag-font">
                         Bengaluru:
                     </p>
                     <p class="footer-p-tag">4th Floor, Salarpuria Towers,No 22, Hosur Main road ,
                     </p>
                     <p class="footer-p-tag">6th Block, Koramangala, Bengaluru - 560034</p>
                     <p class="footer-p-tag">Phone: 09742087612</p>                     
                 </div>
            </div>
            <div class="container">
                <div class="col-md-12 col-xs-12 col-lg-12">
                    <p class="footer-p-tag footer-p-tag-font" style="  padding-top: 30px;  font-weight: 600 !important;">Around the Web</p>                    
                </div>
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <ul class="socialMediaIcons">
                        <li class="facebookIcon socialLinks">
                            <a href="https://www.facebook.com/stones2milestones/">
                                <div class="fb"></div>
                            </a>
                        </li>
                        <li class="linkedIn socialLinks">
                            <a href="https://www.linkedin.com/company-beta/1974028/">
                                <div class="linkedin"></div>
                            </a>
                        </li>
                        <li class="blog socialLinks">
                            <a href="http://stones2milestones.com/blog">
                                <div class="blog"></div>
                            </a>
                        </li>
                        <li class="twitter socialLinks">
                            <a href="https://twitter.com/S2MTweets">
                                <div class="twitter"></div>
                            </a>
                        </li>
                        <li class="google socialLinks">
                            <a href="#">
                                <div class="googleplus"></div>
                            </a>
                        </li> 
                    </ul>
                </div>
            </div>
        </div> -->
        <div class="footer-below-two">
            <div class="container">
                <div class="col-lg-4 col-md-4 col-xs-12">
                    <p class="footer-p-tag allrightreserved">From 2008 Stones2Milestones All Rights Reserved</p>
                </div>
                <!-- <div class="col-md-6 col-lg-6 col-xs-12">
                    <p class="footer-p-tag">Privacy Policy | Terms of Service | Security & Compliance | Sitemap </p>
                </div> -->
            </div>
        </div>
        <div class="footertagline">
            <div class="col-md-12 col-lg-12 col-xs-12 footertagline-div">
                <img class="txt_img" src="img/worded-with-care.svg" alt="">
            </div>
        </div>
    </footer>
    
    <div class="modal fade" id="myModalthankyou" role="dialog">
                          <div class="modal-dialog">
                          
                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">×</button>
                                
                              </div>
                              <div class="modal-body">
                                 <h1 class="thankyou">Thank you! We shall get in touch shortly.</h1>
                              </div>
                            </div>
                            
                          </div>
      </div>
    <!-- Plugin JavaScript -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="https://cdn.firebase.com/js/client/2.2.0/firebase.js"></script> 
    

    <!-- <script type="text/javascript">
        $(document).ready(function(){
            $('.fadeInUpdiv').addClass('fadeInUp');    
        });
        
    </script> -->
    <script type="text/javascript">
        $('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 4000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});
        
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
  var owl = $('#owl2');
  owl.owlCarousel({
    items:4,
    loop:true,
    margin:10,
    autoplay:true,
    
    autoplayHoverPause:false,
    autoplayHoverPause:false,
        responsive:{
        0:{
            items:2
        },
        600:{
            items:3
        },
        1000:{
            items:4
        },
        1300:{
            items:4
        }
    }
});
});
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
  var owl = $('#owl1');
  owl.owlCarousel({
    items:3,
    loop:true,
    margin:10,
    autoplay:true,
    
    autoplayHoverPause:false,
        responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:3
        },
        1300:{
            items:3
        }
    }
});
});
    </script>
    <script type="text/javascript">
        $(document).ready(function(){

    var native_width = 0;
    var native_height = 0;
  $(".large").css("background","url('" + $(".small").attr("src") + "') no-repeat");

    //Now the mousemove function
    $(".magnify").mousemove(function(e){
        //When the user hovers on the image, the script will first calculate
        //the native dimensions if they don't exist. Only after the native dimensions
        //are available, the script will show the zoomed version.
        if(!native_width && !native_height)
        {
            //This will create a new image object with the same image as that in .small
            //We cannot directly get the dimensions from .small because of the 
            //width specified to 200px in the html. To get the actual dimensions we have
            //created this image object.
            var image_object = new Image();
            image_object.src = $(".small").attr("src");
            
            //This code is wrapped in the .load function which is important.
            //width and height of the object would return 0 if accessed before 
            //the image gets loaded.
            native_width = image_object.width;
            native_height = image_object.height;
        }
        else
        {
            //x/y coordinates of the mouse
            //This is the position of .magnify with respect to the document.
            var magnify_offset = $(this).offset();
            //We will deduct the positions of .magnify from the mouse positions with
            //respect to the document to get the mouse positions with respect to the 
            //container(.magnify)
            var mx = e.pageX - magnify_offset.left;
            var my = e.pageY - magnify_offset.top;
            
            //Finally the code to fade out the glass if the mouse is outside the container
            if(mx < $(this).width() && my < $(this).height() && mx > 0 && my > 0)
            {
                $(".large").fadeIn(100);
            }
            else
            {
                $(".large").fadeOut(100);
            }
            if($(".large").is(":visible"))
            {
                //The background position of .large will be changed according to the position
                //of the mouse over the .small image. So we will get the ratio of the pixel
                //under the mouse pointer with respect to the image and use that to position the 
                //large image inside the magnifying glass
                var rx = Math.round(mx/$(".small").width()*native_width - $(".large").width()/2)*-1;
                var ry = Math.round(my/$(".small").height()*native_height - $(".large").height()/2)*-1;
                var bgp = rx + "px " + ry + "px";
                
                //Time to move the magnifying glass with the mouse
                var px = mx - $(".large").width()/2;
                var py = my - $(".large").height()/2;
                //Now the glass moves with the mouse
                //The logic is to deduct half of the glass's width and height from the 
                //mouse coordinates to place it with its center at the mouse coordinates
                
                //If you hover on the image now, you should see the magnifying glass in action
                $(".large").css({left: px, top: py, backgroundPosition: bgp});
            }
        }
    })
});
    </script>
    <script type="text/javascript">
        var tabCarousel = setInterval(function() {
    var tabs = $('#yourTabWrapper .nav-tabs > li'),
        active = tabs.filter('.active'),
        next = active.next('li'),
        toClick = next.length ? next.find('a') : tabs.eq(0).find('a');

    toClick.trigger('click');
}, 20000);
    </script>
    <script type="text/javascript">
        $('#myModal1').on('shown.bs.modal', function () {
  $('#myInput').focus()
})
    </script>
    <!-- <script type="text/javascript">
      $(document).ready(function(){
        $('#myCarousel').carousel({
            interval: 4000,
            cylce: true
        })
        $('.carousel .item').each(function () {
            var next = $(this).next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }
            next.children(':first-child').clone().appendTo($(this));
            if (next.next().length > 0) {
                next.next().children(':first-child').clone().appendTo($(this));
            } else {
                $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
            }
        });
    });

    </script>
     --><!-- <script src="js/jquery.bxslider.min.js"></script> -->
     <!-- jQuery -->
    <script src="lib/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
       <script src="lib/bootstrap/js/bootstrap.min.js"></script>
       <script type="text/javascript" src="js/YouTubePopUp.jquery.js"></script>
    <!-- Latest compiled and minified JavaScript -->
   <!--  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
 -->
    
    <!-- <script src="js/jquery.elevatezoom.js"></script> -->
    <!-- Theme JavaScript -->
     
     <script src="js/owl.carousel.min.js"></script>
     
     <script src="js/new-age.min.js"></script>
     <!-- <script type="text/javascript">
         $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
jQuery(function(){
          jQuery("a.demo").YouTubePopUp().click();
          // jQuery("a.demo").YouTubePopUp({ autoplay: 0 }); // Disable autoplay
      });
     </script> -->

     <!-- Start of Async Drift Code -->
<script>
!function() {
  var t;
  if (t = window.driftt = window.drift = window.driftt || [], !t.init) return t.invoked ? void (window.console && console.error && console.error("Drift snippet included twice.")) : (t.invoked = !0, 
  t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ], 
  t.factory = function(e) {
    return function() {
      var n;
      return n = Array.prototype.slice.call(arguments), n.unshift(e), t.push(n), t;
    };
  }, t.methods.forEach(function(e) {
    t[e] = t.factory(e);
  }), t.load = function(t) {
    var e, n, o, i;
    e = 3e5, i = Math.ceil(new Date() / e) * e, o = document.createElement("script"), 
    o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + i + "/" + t + ".js", 
    n = document.getElementsByTagName("script")[0], n.parentNode.insertBefore(o, n);
  });
}();
drift.SNIPPET_VERSION = '0.3.1';
drift.load('z5525s5rhhu9');
</script>
<!-- End of Async Drift Code -->

<script type="text/javascript">
var dbRef = new Firebase("https://s2m-database.firebaseio.com/");

var contactsRef = dbRef.child('contacts')

//save contact

$('.addValue').on("click", function( event ) {  

    event.preventDefault();

    if( $('#formName').val() == '' || $('#formEmail').val() == '' || $('#formPhone').val() == ''  ){
        alert('Please Enter Name, Mobile Number and Email!');
    }
    else {
      console.log($('#name').val() )    
      contactsRef

        .push({

          name: $('#formName').val(),

          email: $('#formEmail').val(),

         

            formPhone: $('#formPhone').val(),

            formText: $('#formText').val(),

            selectParentTeacher: $('#selectParentTeacher').val()

          

        })

        //contactForm.reset();
        $('#myModal').modal('toggle');
        $('#myModalthankyou').modal('toggle');

    }

  });

      
</script>

<script type="text/javascript">
  var dbRef = new Firebase("https://s2m-database.firebaseio.com/");

var emailaddress = dbRef.child('signups')

$('.submitsignup').on("click", function( event ) {

 event.preventDefault();

    if( $('#email').val() != ''  ){

      emailaddress

        .push({

          email: $('#MERGE0').val(),

        })

        contactForm.reset();

    } else {

      alert('Please Enter Name, Mobile Number and Email!');

    }


});
</script>
<script type="text/javascript">
    $(window).on("scroll", function () {
    if ($(this).scrollTop() > 70) {
        $(".exp-us").addClass("color-change");
        $(".p-tag-exp").addClass("color-change-p");

        $('#phone-logo img').attr('src', 'img/phone-call.svg');

    }
    else {
        $(".exp-us").removeClass("color-change");
        $(".p-tag-exp").removeClass("color-change-p");
        $('#phone-logo img').attr('src', 'img/phonecall.svg');
    }
});
</script>
<script type="text/javascript">
  function sticky_relocate() {
    var window_top = $(window).scrollTop();
    var div_top = $('#sticky-anchor').offset().top - 5;
    var div_section_1 =$('#visions2m').offset().top - 5;
    var div_section_2 =$('#missions2m').offset().top - 5;
    var div_section_3 =$('#corevaluess2m').offset().top - 20;
    var div_section_4 =$('#teams2m').offset().top;
    //console.log(window_top);
    if (window_top > div_top) {
        $('#sticky').addClass('stick');
        
        
        $('#stickyHeading').addClass('stickHeading'); 
        $('#stickyHeading1').addClass('stickHeading');        
        $('#stickyHeading2').addClass('stickHeading'); 
        $('#stickyHeading3').addClass('stickHeading'); 
        $('#sticky-anchor').height($('#sticky').outerHeight());

    } else {
        $('#sticky').removeClass('stick');
        
        $('#sticky-anchor').height(0);
    }

    

    if (window_top > div_section_1 && window_top < div_section_2){
      //console.log(div_section_1,div_section_2,div_section_3,div_section_4);
      $('#stickyli1').addClass('stickli');
      $('#stickyli2').removeClass('stickli');
      $('#stickyli3').removeClass('stickli');
      $('#stickyli4').removeClass('stickli');
    } else if(window_top > div_section_2 && window_top < div_section_3 ){
      $('#stickyli2').addClass('stickli');
      $('#stickyli1').removeClass('stickli');
      $('#stickyli4').removeClass('stickli');
      $('#stickyli3').removeClass('stickli');
    }
    else if(window_top > div_section_3 && window_top < div_section_4 ){
      $('#stickyli3').addClass('stickli');
      $('#stickyli2').removeClass('stickli');
      $('#stickyli1').removeClass('stickli');
      $('#stickyli4').removeClass('stickli');
    }
    else if(window_top > div_section_4){
      $('#stickyli4').addClass('stickli');
      $('#stickyli3').removeClass('stickli');
      $('#stickyli2').removeClass('stickli');
      $('#stickyli1').removeClass('stickli');
    }
}

$(function() {
    $(window).scroll(sticky_relocate);
    sticky_relocate();
});

</script>

 <script> (function() { var qs,js,q,s,d=document, gi=d.getElementById, ce=d.createElement, gt=d.getElementsByTagName, id="typef_orm_share", b="https://embed.typeform.com/"; if(!gi.call(d,id)){ js=ce.call(d,"script"); js.id=id; js.src=b+"embed.js"; q=gt.call(d,"script")[0]; q.parentNode.insertBefore(js,q) } })() </script>
</body>

</html>
