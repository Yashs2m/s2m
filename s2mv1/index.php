<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Stones2Milestones is an organisation in the education space that aims to impact the way in which school going children between the ages of 3 to class 3 learn to read in English.">
    <meta name="author" content="">
    <meta name="keywords" content="Reading,children, grade 1 English, grade 2 English, English, child,Novel, learn, learn English, learn reading,stones, stones2milestones, reader, English grammar, language, english Novel, ">
    <title>Stones2Milestones | My place in the world</title>
    <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="img/s2sfavicon.png"/>
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/main.css">   
    <link rel="stylesheet" href="css/modal.css">   
    <link href="css/new-age.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/footer.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/owl.theme.default.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/YouTubePopUp.css">
    <script src="https://cdn.firebase.com/js/client/2.2.0/firebase.js"></script> 
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-81416339-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<style type="text/css">
.typeform-popup-wrapper.typeform-popup-mode-popup{
        width: 50%!important;
    height: 80%!important;
    top: 10%!important;
    left: 25%!important;
      }
.color-change{
    border: 1px solid #39a935;

  }
  .color-change-p{
    color: #39a935;
  }
  .fa{
    color: #39a935;
    font-size: 20px;
  }
  .navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:hover, .navbar-default .navbar-nav > .open > a:focus{
    background: none!important;
    color: white!important;
  }

</style>
<body id="page-top" style="padding-right: 0px!important;">
<!-- Nav Bar starts -->
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span><i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand " href="http://www.stones2milestones.com/">
                    <img src="img/s2s.png" style="width: 150px;">
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                      <li class="dropdown list-view">
                        <a href="#" class="dropdown-toggle page-scroll" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <p>about</p>
                        <ul class="dropdown-menu">
                          <li><a href="http://www.stones2milestones.com/about.php">About</a></li>
                          <li><a href="http://www.stones2milestones.com/about.php#visions2m">Vision</a></li>
                          <li><a href="http://www.stones2milestones.com/about.php#missions2m">Mission</a></li>
                          <li><a href="http://www.stones2milestones.com/about.php#corevaluess2m">Core Values</a></li>
                          <li><a href="http://www.stones2milestones.com/about.php#teams2m">Team</a></li>
                        </ul></a>
                      </li>
                      <li class="dropdown list-view">
                        <a href="#" class="dropdown-toggle page-scroll" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <p>For Schools</p></a>
                        <ul class="dropdown-menu">
                          <li><a href="http://www.wingsofwords.com/">Wings of Words</a></li>
                          <li><a href="https://play.google.com/store/apps/details?id=com.wowconnect">WOWConnect</a></li>
                          <li><a href="http://www.wingsofwords.com/blog">Blog</a></li>
                        </ul>
                      </li>
                      <li class="dropdown list-view">
                        <a href="#" class="dropdown-toggle page-scroll" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <p>For Parents</p></a>
                        <ul class="dropdown-menu">
                          <li><a href="https://www.getfreadom.com/">Freadom</a></li>
                          <li><a href="https://play.google.com/store/apps/details?id=com.application.freadom&hl=en">Download App</a></li>
                          <li><a href="https://getfreadom.com/blog/">Blog</a></li>
                        </ul>
                      </li>
                      <li class="list-view mobile-view">
                        <a class="page-scroll" href="http://www.stones2milestones.com/about.php">
                            <p>About</p>
                        </a>
                      </li>
                      <li class="list-view mobile-view">
                        <a class="page-scroll" href="http://www.stones2milestones.com/partners.php">
                            <p>Partner With Us</p>
                        </a>
                      </li><li class="list-view mobile-view">
                        <a class="page-scroll" href="http://www.wingsofwords.com/">
                            <p>For Schools</p>
                        </a>
                      </li><li class="list-view mobile-view"> 
                        <a class="page-scroll" href="https://play.google.com/store/apps/details?id=com.wowconnect">
                            <p>WOWConnect</p>
                        </a>
                      </li><li class="list-view mobile-view">
                        <a class="page-scroll" href="https://www.getfreadom.com/">
                            <p>For Parents</p>
                        </a>
                      </li><li class="list-view mobile-view" >
                        <a class="page-scroll" href="https://play.google.com/store/apps/details?id=com.application.freadom&hl=en">
                            <p>Download the App Freadom</p>
                        </a>
                      </li>
                      <li class="list-view">
                        <a class="page-scroll" href="http://www.stones2milestones.com/blog/">
                            <p>Blog</p>
                        </a>
                      </li>
                     <li class="dropdown list-view">
                        <a href="http://www.stones2milestones.com/partners.php" class="dropdown-toggle page-scroll" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <p>Partner With Us</p></a>
                        <ul class="dropdown-menu">
                          <li><a href="http://www.stones2milestones.com//partners.php">Partners</a></li>
                          <li><a href="http://www.stones2milestones.com//partners.php#engages2m">How Do We Enagage?</a></li>
                          <li><a href="http://www.stones2milestones.com//partners.php#partnerss2m">Partner Schools</a></li>
                          <li><a href="http://www.stones2milestones.com//partners.php#csrs2m">Social Impact</a></li>
                          <li><a href="http://www.stones2milestones.com//partners.php#testimonials2m">Testimonial</a></li>
                        </ul>
                      </li>
                      <li class="exp-us list-view">
                        <!-- <a class="page-scroll exp-us-a" data-toggle="modal" data-target="#myModal">
                            <p class="p-tag-exp">Join the mission</p>
                        </a> -->
                        <a class="typeform-share button" href="https://stones2milestones.typeform.com/to/ksK99C" data-mode="popup" style="padding: 6px;" target="_blank"><p class="p-tag-exp" style="margin-bottom: 0px;
    text-transform: uppercase;    font-size: 10px;">Join the mission</p></a>
                    </li>
                    <!-- <li>
                      <button type="button" class="btn btn-info btn-lg" >Open Modal</button>
                    </li> -->
                    <!-- Modal -->

                        
                    <li class="phone">
                        <span id="phone-logo"><img src="img/phonecall.svg" style="margin-right:5px; width: 20px;">+91 90770 77777</span>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
<div class="modal fade" id="myModal" role="dialog">
                          <div class="modal-dialog">
                          
                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">×</button>
                                <h4 class="modal-title">Join The Mission</h4>
                              </div>
                              <div class="modal-body">
                              <div role="form" class="wpcf7" id="wpcf7-f433-o1" lang="en-US" dir="ltr">
                      <div class="screen-reader-response"></div>
                      <form action="" method="post" id= "newActivity" name="newActivity" class="" novalidate="novalidate">
                      <div style="display: none;">
                      <input type="hidden" name="_wpcf7" value="433">
                      <input type="hidden" name="_wpcf7_version" value="4.5.1">
                      <input type="hidden" name="_wpcf7_locale" value="en_US">
                      <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f433-o1">
                      <input type="hidden" name="_wpnonce" value="37e90b15f0">
                      </div>
                      <p><span class="wpcf7-form-control-wrap iam"><select id="selectParentTeacher" name="iam" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required pop-in" aria-required="true" aria-invalid="false"><option value="I am a Parent">I am a Parent</option><option value="I am a Teacher">I am a Teacher</option><option value="I am a School Admin">I am a School Admin</option></select></span><br>
                      <span class="wpcf7-form-control-wrap name">
                      <input type="text" id="formName" name="name" required  value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required pop-in" aria-required="true" aria-invalid="false" placeholder="Your name"></span><br>
                      <span class="wpcf7-form-control-wrap phone">
                      <input type="tel" name="phone" id="formPhone" required value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel pop-in" aria-required="true" aria-invalid="false" placeholder="Mobile number"></span><br>
                      <span class="wpcf7-form-control-wrap email">
                      <input type="email" id="formEmail" name="email" value="" required size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email pop-in" aria-required="true" aria-invalid="false" placeholder="Email address"></span><br>
                      <span class="wpcf7-form-control-wrap school">
                      <input type="text" id="formText"name="school" value="" size="40" class="wpcf7-form-control wpcf7-text pop-in" aria-invalid="false" placeholder="School name (if you are a Teacher or School Admin)"></span><br>
                      <input type="submit" id="saveFrom" value="Request" class="wpcf7-form-control wpcf7-submit popbtn addValue"><img class="ajax-loader" src="" alt="Sending ..." style="visibility: hidden;"></p>
                      <div class="wpcf7-response-output wpcf7-display-none"></div></form></div>        </div>
                            </div>
                            
                          </div>
</div>


<style type="text/css">
      .header-overlay{
        
      }
      
    </style>

<!-- Nav Bar Ends-->
    <video poster="img/home1.jpg" id="bgvid" playsinline autoplay muted loop>
    <source src="img/video.mp4" type="video/mp4" id="source-id">
    </video>
    <header class="polina">
    <div class="header-overlay">
      <div class="container">
            <div class="can">
                <h2>Creating A Nation </h2>
                <br>
                <h1>Of Readers</h1>
                <!-- <div class="play-media">
                  <a href="https://www.youtube.com/watch?v=ygwRc9WuBBM" class="demo" data-toggle="tooltip" title="" data-original-title="Personal Message From The Founder!">
                  <img src="img/playsvgmedia.svg">
                  </a>
                </div> --> 
            </div>                
       </div>
    </div>
        
    </header>
    <section id="products" class="products text-center">
        <div class="container">
            <div class="outer-products">
                <div class="col-lg-12 col-sm-12 col-md-12">
                    <div class="col-lg-3 col-md-3 col-sm-3 inner-products">
                        <a href="http://www.wingsofwords.com/">
                            <img src="img/wowlogosvg.svg" style="width: 70px; padding-top: 10px;">    
                        </a>                        
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 inner-products">
                        <a href="https://getfreadom.com/">
                            <img src="img/freadomlogotag.svg" style="width: 110px; padding-top: 20px;">
                        </a>                        
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 inner-products">
                        <img src="img/fasstlogov1.svg" style="width: 60px;     padding-top: 15px;; ">
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 inner-products">
                        <a href="https://play.google.com/store/apps/details?id=com.wowconnect">
                            <img src="img/wowconnectlogofinal.svg" style="width: 75px;">    
                        </a>                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="home" class="home bg-primary text-center parallax"> 
        <div class="container text-center mobile-bg">
          <div class="col-xs-12">
            <div class="col-xs-12">
              <!-- <h1 class="heading-tag-mobile">Creating A Nation Of Readers</h1> -->

            </div>
            <div class="col-xs-12 viedo-bg" style="display: none;">
              <a href="https://www.youtube.com/watch?v=ygwRc9WuBBM" class="demo" data-toggle="tooltip" title="" data-original-title="Personal Message From The Founder!">
                  <img src="img/playsvgmedia.svg" style="    width: 40px;padding-bottom: 20px;"></a>
            </div>
          </div>
        </div>       
        <div class="container text-center">
            <div >
               <div class="col-sm-12 col-md-12 col-lg-12 heading">
                   <h1>
                       Why Is Reading So Important?
                   </h1>
               </div> 
               <div class="col-sm-12 col-md-12 col-lg-12 sub-heading">
                   <h3>Quite simply, reading is magical</h3>
               </div>
               <div class="col-sm-12 col-md-12 col-lg-12 content-div">
                   <p>When taught well, it adds to one’s imagination, logical thinking, visualisation, decoding, writing, comprehension, analysis and creative thinking skills. Children who read have an unabashed curiosity, a certain academic edge over their peers and an advanced understanding of the world.</p>
               </div>
            </div>  
        </div>
        <div class="container" style="padding-top: 30px;">
            <div class="yourTab" id="yourTabWrapper">
              <ul class="nav nav-tabs mynavtabs" id="myTab" >
              <li class="active"><a data-target="#readby" data-toggle="tab">Reading by Chance</a></li>
              <li><a data-target="#schoolengage" data-toggle="tab">Reading by Design</a></li>
              <li><a data-target="#ourapproach" data-toggle="tab">Our Approach</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="readby">
                  <div class="container">
                      <div class="col-md-12 col-lg-12 col-sm-12">                          
                          <div class="col-sm-6 col-lg-6 col-md-6 " >
                              <div class="img-center-div">
                                  <img src="img/reading-290x300.png">
                              </div>
                          </div>
                          <div class="col-sm-6 col-lg-6 col-md-6 content-readby" >
                              <div class="">
                                  <div class="" >
                                 <h5 class="row-head"> Reading when left to chance</h5>
                              </div>
                              <div class=" ">
                                  <h5 class="row-head-content">INDIA COMPARES POORLY</h5>                         
                                  <h5 class="row-content">
                                  In the latest PISA reading assessment, of the 74 regions participating, India outscored only Kyrgyzstan.</h5>
                              </div>
                              </div>
                              
                              <div class="">
                                  <h5 class="row-head-content">NO PROGRESS IN READING LEVELS</h5>                         
                                  <h5 class="row-content">A child’s ability to read English has remained unchanged in lower primary grades over the years, with only 25% of children in grade 5 being able to read simple sentences in English.</h5>
                              </div>
                              <div class="">
                                  <h5 class="row-head-content">NOT READING FOR PLEASURE</h5>                         
                                  <h5 class="row-content">In 1984, 8% of 13-year-olds and 9% of 17-year-olds said they “hardly ever” read for pleasure. In 2014, that number had almost tripled, to 22% and 27%</h5>
                              </div>
                            </div>                          
                      </div>
                  </div>
              </div>
              <div class="tab-pane" id="ourapproach">
                  <div class="container">
                      <div class="col-md-12 col-lg-12 col-xs-12">
                          <h5 class="row-head"> Reading Triangle</h5>
                          <img src="img/pencile.png" style="width: 400px;">
                          <h5 class="row-content">The hallmark of our program is that all the instructional strategies are embedded into a matrix of rich, meaningful, age appropriate and engaging literature created in-house. The entire program is based on our Reading Triangle.</h5>
                      </div>
                  </div>
              </div>
              <div class="tab-pane" id="schoolengage">
                  <div class="container">
                      <div class="col-md-12 col-lg-12 col-sm-12">                          
                          <div class="col-sm-6 col-lg-6 col-md-6 content-readby" >
                              <div class="">
                                  <div class="" >
                                 <h5 class="row-head"> Principles to make it happen by DESIGN</h5>
                              </div>
                              <div class=" ">
                                  <h5 class="row-head-content">START EARLY</h5>                         
                                  <h5 class="row-content">Research says that age 3 to class 3 is the magical window to teach reading.</h5>
                              </div>
                              </div>
                              <div class="">
                                  <h5 class="row-head-content">SYSTEMATIC & EXPLICIT APPROACH</h5>                         
                                  <h5 class="row-content">Skills and concepts are taught in a planned, logically progressive and engaging sequence. </h5>
                              </div>
                              <div class="">
                                  <h5 class="row-head-content">SUSTAINED EFFORT</h5>                         
                                  <h5 class="row-content">As with any other habit formation, it takes 7 years for a habit to become a part of one's DNA.</h5>
                              </div>
                            </div>
                          <div class="col-sm-6 col-lg-6 col-md-6 " >
                              <div class="img-center-div">
                                  <img src="img/readbydesign.svg" style="width: 400px; height: 400px;">
                              </div>
                          </div>                          
                      </div>
                  </div>
              </div>
              
            </div>
            </div>
        </div>
        <div class="container" style="">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class=" heading ">
                    <h1 class="heading-desc">
                        What Happens When You Use Our Products
                    </h1>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-3 div-we-are">
                    <div class="">
                        <img src="img/sound.png">
                    </div>
                    <div class="fadeInUpdiv animated fadeInUp">
                        <h5 class="div-we-are-content">The child’s WOW journey starts with the world of sounds and building skills like visualisation, logical thinking, spelling expertise, writing expertise and an increased vocabulary along the way.</h5>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-3 div-we-are">
                    <div class="">
                        <img src="img/reading-1.png">
                    </div>
                    <div class="fadeInUpdiv animated fadeInUp">
                        <h5 class="div-we-are-content">Over the years, with respect to reading, children are able to enjoy controlled text independently.</h5>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-3 div-we-are">
                    <div class="">
                        <img src="img/child.png">
                    </div>
                    <div class="fadeInUpdiv animated fadeInUp">
                        <h5 class="div-we-are-content">Along with reading the program helps improve the child’s comprehension, listening, speaking  and writing skills.</h5>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-3 div-we-are">
                    <div class="">
                        <img src="img/teacher.png">
                    </div>
                    <div class="fadeInUpdiv animated fadeInUp">
                        <h5 class="div-we-are-content">The science and structure of the program touches the teacher at the core making her an emotionally intelligent educator and developing her language skills.</h5>
                    </div>
                </div>
            </div>         
        </div>
        <div class="container" style="">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 30px;">
                    <div class=" heading ">
                    <h1 class="heading-desc">
                        Our Offerings
                    </h1>
                </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6 boxstyle">
                <a href="http://www.wingsofwords.com/">
                    <div class="inner-box">
                        <div class="inner-box-content">
                            <img class="text-center" src="img/wowlogosvg.svg" alt="wingsofwords" width="115px" >
                        </div>
                        <!-- <h4>WINGS OF WORDS</h4> -->
                        <h5>
                       Wings of Words is an in-school reading instruction program to build the will and skill to read in children of ages 3 to class 3 to improve english as a language.</h5>
                        <h5 class="knowmore-wow">LEARN MORE</h5>
                    </div>
                </a>
                    
                    <!-- <p style="text-align: left;"><a href="https://play.google.com/store/apps/details?id=com.application.freadom" target="_blank"><img src="img/googleplay.jpg"></a></p> -->
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6 boxstyle">
                <a href="https://getfreadom.com/">
                    <div class="inner-box">
                        <div class="inner-box-content">
                            <img class="text-center" src="img/FullLogo-Tagline.png" alt="FREADOM" width="200px" >
                        </div>
                        <!-- <h4>FREADOM</h4> -->
                        <h5>Designed to help you raise a happy reader.Freadom is reading made fun. It brings together the parent and the child for a neat 15-minute bundles of fun.</h5>
                        <h5 class="knowmore-freadom">GET THE APP</h5>
                    </div>
                    <!-- <p style="text-align: left;"><a href="https://play.google.com/store/apps/details?id=com.application.freadom" target="_blank"><img src="img/googleplay.jpg"></a></p> -->
                </a>
                    
                </div>
            </div>
            <div class="col-xs-12 col-lg-12 col-md-12" style="padding-top: 40px;">
               
                <div class="col-lg-6 col-md-6 col-xs-6 boxstyle">
                <a href="https://play.google.com/store/apps/details?id=com.wowconnect">
                    <div class="col-md-6 col-lg-6 col-xs-12 boxstyle-div-fast-wow">
                        <div>
                            <img src="img/wowconnectlogofinal.svg" class="wow-img" style="width:150px; ">
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xs-12 boxstyle-div-fast-wow">
                        <div>
                        <h5 class="div-we-are-content">WOW Connect is a web and mobile application for teachers to prepare for sessions, monitor progress, build network & train on the go to raise happy readers in the classroom. </h5>
                        </div>
                    </div>
                   </a>
                </div>
                 <div class="col-lg-6 col-md-6 col-xs-6 boxstyle">

                    <div class="col-md-6 col-lg-6 col-xs-12 boxstyle-div-fast-wow">
                        <div>
                            <img src="img/fastlogo.png" class="fast-img" style="width: 200px; padding: 35px; ">
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xs-12 boxstyle-div-fast-wow">
                        <div>
                        <h5 class="div-we-are-content">FAST is India’s first standardized Reading Assessment which estimates a child’s reading level and throws light on what is needed to move ahead on their reading journey.</h5>
                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="container" style="padding-top: 50px;">
            <div class="col-lg-12 class-magnify" >
                <!-- Lets make a simple image magnifier -->
                <div class="magnify">
                    
                    <!-- This is the magnifying glass which will contain the original/large version -->
                    <div class="large"></div>
                    
                    <!-- This is the small image -->
                    <img class="small" src="img/s2meco.png" style="width: 100%; height: auto;"/>
    
                </div>
            </div>
        </div>        
        <div class="container" style="">
            <div class="col-lg-12">
                <div class=" heading ">
                    <h1 class="heading-desc">
                       Here's What Some People Think About Us
                    </h1>
                </div>
                <div class="owl-carousel owl-theme" id="owl1">
                <div class="item">
                    <blockquote class=""> 
                        <h5>Thank you so much for a book which could break the monotony of our classroom. Learning has become very interesting, the content of this book is impeccable, the students are doing the exercises, especially blends with great interest. <span class="bottom-arrow"></span></h5>
                        <div class="image-icon has-bg" style="background-image: url(img/round12.png);"></div>
                        <span>Romia Sharma </span>
                        <span class="title">DPS, Amritsar</span>
                    </blockquote>
                </div>
                <div class="item">
                    <blockquote class=""> 
                        <h5> It has been a very good experience teaching students with help of Wings of Words. They enjoyed a lot while working out the exercises at the back. Fun activities, blend words, compound words were the favorite topics for the students.<span class="bottom-arrow"></span></h5>
                        <div class="image-icon has-bg" style="background-image: url(img/round12.png);"></div>
                        <span>Paramita Bardhan</span>
                        <span class="title"> Edify School, Bengaluru</span>
                    </blockquote>
                </div>
                <div class="item">
                    <blockquote class=""> 
                        <h5>My child won the Nation Spell Bee competition because of his development which happened through Wings of Words. He used to be a shy child who was not confident. He is more aware of himself now. <span class="bottom-arrow"></span></h5>
                        <div class="image-icon has-bg" style="background-image: url(img/round02.png);"></div>
                        <span>Mr. P. K. Chaubey</span>
                        <span class="title">Parent of Wings of Words Student</span>
                    </blockquote>
                </div>
            </div>
            </div>           
        </div>
        <section class="success fr_sec yellow newsletter_sec">
          <div class="container">
              <div class="row">
                  <div class="newsletter_holder col-xs-12">
                      <div class="newsletter_heading col-xs-12">
                          <h4 class="newsletter_title">Sign up to receive amazing stuff around reading</h4>
                          <!-- <h5 class="newsletter_subtitle">Delivered weekly straight into your inbox</h5> -->
                      </div>
                      <div class="newsletter_form col-xs-12 col-xs-offset-0 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                          <form action="contact.php" method="POST" accept-charset="utf-8" class="flex_holder" style=" border: 2px solid #0e6837;box-shadow: 0px -1px 10px -3px;">
                              <!-- <input type="hidden" name="u" value="3eaaed45454f13a1c2c555c5a"> -->
                              <input type="hidden" name="list" value="joNJFfKW2v484XtmxrUEtA"/>
                              <!-- <input type="hidden" name="id" value="cc047a2e71"> -->
                              <div class="flex_elements input_with_icon">
                              <i class="fa fa-paper-plane" aria-hidden="true"></i>
                                  <input type="email" autocapitalize="off" autocorrect="off" name="MERGE0" id="MERGE0" size="25" value="" class="inputtext" placeholder="Enter your Email">
                              </div>
                              
                              <a href="#contactModalsign" role="button"  data-toggle="modal">
                              <input type="submit"  name="submit" id="submit" class="flex_elements btn btn-primary submitsignup" value="Sign Up">
                              </a>
                              <div id="contactModalsign" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                      <!-- <h3 id="myModalLabel">Almost finished...</h3> -->
                                    </div>
                                    <div class="modal-body">
                                     <div class="s2mblack" style="padding: 20px;">
                                         <img src="img/s2mblack.png">
                                     </div>
                                     <div style=" padding: 30px;text-align:center; margin-top: -80px;">
                                         <p>Thank you for subscribing!</p>
                                     </div>
                                    </div>
                                    <!-- <div class="modal-footer" style="border: none;">
                                      <button class="btn" data-dismiss="modal" aria-hidden="true" style="float: left; margin-left: 44%;">Cancel</button>
                                    </div>
                                   --></div>
                                </div>
                              </div>   
                          </form>
                      </div>
                  </div>
              </div>
          </div>

        </section>
        <div class="container" style="padding-top: 60px; padding-bottom: 40px; ">
            <!-- Set up your HTML -->
            <div class="owl-carousel owl2-style" id="owl2">
                
                
                <a href="#"><img src="img/youwecan.png" alt="logo05" title="logo05" style="width:150px!important;"></a>
                <a href="#"><img src="img/orchid.png" alt="logo05" title="logo05"></a>
                <a href="#"><img src="img/logo02-2.jpg" alt="logo05" title="logo05"></a>
                <a href="#"><img src="img/logo06-1.jpg" alt="logo05" title="logo05"></a>
                <a href="#"><img src="img/manyavar.png" alt="logo05" title="manyavar" style="width: 150px!important;"></a>
                <a href="#"><img src="img/inclusive-india-logo.png" alt="blackstone-logo" title="blackstone-logo" style="width:150px!important;padding-top: 10px;"></a>
                <a href="#"><img src="img/logo03-1.jpg" alt="logo05" title="logo05" style="width:150px!important;padding-top: 5px;"></a>   
                <a href="#"><img src="img/teach4india.png" alt="logo05" title="logo05" style="width: 180px!important; padding-top: 30px;"></a>                        
            </div>
        </div>
        </section>
    <section class="school-count">
         <div class="container">
            <div class="col-md-12 col-sm-12 col-lg-12 ">
                <div class="col-xs-4 col-md-4 col-lg-4 school-count-div">
                <div class="col-xs-6 col-md-6 col-lg-6">
                    <img src="img/01png.png" class="school-count-width">
                </div>
                <div class="col-md-6 col-lg-6 col-xs-6 school-count-tag">
                <div class="row count">
                    120
                </div>
                <div class="row">
                    <h3 class="school-tag">SCHOOLS</h3>
                </div>
                </div>
                </div>
                <div class="col-xs-4 col-md-4 col-lg-4 school-count-div">
                <div class="col-xs-6 col-md-6 col-lg-6">
                    <img src="img/02png.png" class="school-count-width">
                </div>
                <div class="col-md-6 col-lg-6 col-xs-6 school-count-tag">
                <div class="row count">
                700
                </div>
                <div class="row">
                    <h3 class="school-tag">TEACHERS</h3>
                </div>
                </div>
                </div>
                <div class="col-xs-4 col-md-4 col-lg-4 school-count-div">
                <div class="col-xs-6 col-md-6 col-lg-6">
                    <img src="img/03png.png" class="school-count-width">
                </div>
                <div class="col-md-6 col-lg-6 col-xs-6 school-count-tag">
                <div class="row count">
                   30000
                </div>
                <div class="row">
                    <h3 class="school-tag">STUDENTS</h3>
                </div>
                </div>
                </div>
            </div>
        </div>
    </section>
     <footer>
        <div class="footer-above">
            <div class="container">
                <div class="col-xs-12 col-md-4 col-lg-4">
                    <a href="http://www.stones2milestones.com/"><img class="img-responsive" src="img/s2s.png" style="width: 200px;padding-bottom: 10px;"></a>
                    <p class="footer-p-tag">
                        Creating a nation of 10 million readers by 2022                    
                    </p>
                     <p class="footer-p-tag">                        
                        We are an organisation in the education space <br> that aims to impact the way in which children <br> learn to read in English.
                    </p>
                    <div class="col-lg-12 col-md-12 col-xs-12" style="padding-left: 0px;">
                    <ul class="socialMediaIcons">
                        <li class="facebookIcon socialLinks">
                            <a href="https://www.facebook.com/stones2milestones/">
                                <div class="fb"></div>
                            </a>
                        </li>
                        <li class="linkedIn socialLinks">
                            <a href="https://www.linkedin.com/company-beta/1974028/">
                                <div class="linkedin"></div>
                            </a>
                        </li>
                        <li class="blog socialLinks">
                            <a href="http://stones2milestones.com/blog">
                                <div class="blog"></div>
                            </a>
                        </li>
                        <li class="twitter socialLinks">
                            <a href="https://twitter.com/S2MTweets">
                                <div class="twitter"></div>
                            </a>
                        </li>
                        <!-- <li class="google socialLinks">
                            <a href="#">
                                <div class="googleplus"></div>
                            </a>
                        </li>  -->
                    </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-md-2 col-lg-2 ">
                    
                   
                        <p class="footer-p-tag-head">Company</p>
                        <p class="footer-p-tag"><a href="http://www.stones2milestones.com/about.php">About</a>  </p>
                        <p class="footer-p-tag"><a href="mailto:work@stones2milestones.com" > Careers</a> </p>
                        <p class="footer-p-tag"><a href="http://www.stones2milestones.com/partners.php">Partner With Us</a></p>
                        <!-- <p class="footer-p-tag"><a href="mailto:work@stones2milestones.com" >Contact Us</a></p> -->
                        <p class="footer-p-tag"><a href="http://www.stones2milestones.com/blog">Blog</a></p>
                        <p class="footer-p-tag"><a href="http://www.stones2milestones.com/policy.php">Policy</a></p>
                   
                </div>
                <div class="col-xs-12 col-md-2 col-lg-2 ">
                   
                        <p class="footer-p-tag-head">For Schools</p>
                        <p class="footer-p-tag"><a href="http://www.wingsofwords.com/">Wings of Words</a></p>
                        <p class="footer-p-tag"><a href="https://play.google.com/store/apps/details?id=com.wowconnect">WOW Connect</a></p>
                        <p class="footer-p-tag"><a href="http://www.wingsofwords.com/blog">Blog</a></p>
                        <p class="footer-p-tag"><a href="https://play.google.com/store/apps/details?id=com.wowconnect"><img src="img/googleplay.png" class="footer-google"></a></p>

                        
                   
                </div>
                <div class="col-xs-12 col-md-2 col-lg-2 ">
                   
                        <p class="footer-p-tag-head">For Parents</p>
                        <p class="footer-p-tag"><a href="http://www.getfreadom.com/">Freadom</a></p>
                        <p class="footer-p-tag"><a href="https://play.google.com/store/apps/details?id=com.application.freadom&hl=en">Download App</a></p>
                        <p class="footer-p-tag"><a href="http://www.getfreadom.com/blog">Blog</a></p>
                        <p class="footer-p-tag"><a href="https://play.google.com/store/apps/details?id=com.application.freadom&hl=en"><img src="img/googleplay.png" class="footer-google"></a></p>
                        
                   
                    <!-- <div class="col-xs-12">
                        <p class="footer-p-tag-head">CONTACT</p>
                        <p class="footer-p-tag">support@stones2milestones.com</p>
                        <p class="footer-p-tag ">Gurgaon </p>
                        <p class="footer-p-tag">419, Tower A, Spaze ITech Park, Sohna Road, Sector 49, Gurgaon - 122018, Haryana</p>
                        <p class="footer-p-tag">Phone: +91 40 4137 1111 </p>
                    </div> -->
                </div>
                <div class="col-xs-12 col-md-2 col-lg-2 ">
                   
                        <p class="footer-p-tag-head">Location</p>
                        <p class="footer-p-tag ">
                         Gurgaon:
                      </p>
                     <p class="footer-p-tag">419, Tower A, Spaze ITech Park, Sohna Road, <br>Sector 49, Gurgaon - 122018, Haryana
                     </p>
                     <!-- <p class="footer-p-tag"></p> -->
                     <p class="footer-p-tag">Phone: +91 90770 77777</p>      
                   
                </div>
            </div>
        </div>
        <!-- <div class="footer-below">
            <div class="container">
                <div class="col-md-12 col-xs-12 col-lg-12">
                    <p class="footer-p-tag-head footer-p-tag-font">Locations</p>
                </div>
                 <div class="col-xs-12 col-md-4">
                     <p class="footer-p-tag footer-p-tag-font">
                         Gurgaon:
                     </p>
                     <p class="footer-p-tag">419, Tower A, Spaze ITech Park, Sohna Road, 
                     </p>
                     <p class="footer-p-tag">Sector 49, Gurgaon - 122018, Haryana</p>
                     <p class="footer-p-tag">Phone: 09742087612</p>                     
                 </div>

                 <div class="col-xs-12 col-md-4">
                     <p class="footer-p-tag footer-p-tag-font">
                         Bengaluru:
                     </p>
                     <p class="footer-p-tag">4th Floor, Salarpuria Towers,No 22, Hosur Main road ,
                     </p>
                     <p class="footer-p-tag">6th Block, Koramangala, Bengaluru - 560034</p>
                     <p class="footer-p-tag">Phone: 09742087612</p>                     
                 </div>

                 <div class="col-xs-12 col-md-4">
                     <p class="footer-p-tag footer-p-tag-font">
                         Bengaluru:
                     </p>
                     <p class="footer-p-tag">4th Floor, Salarpuria Towers,No 22, Hosur Main road ,
                     </p>
                     <p class="footer-p-tag">6th Block, Koramangala, Bengaluru - 560034</p>
                     <p class="footer-p-tag">Phone: 09742087612</p>                     
                 </div>

                 <div class="col-xs-12 col-md-4">
                     <p class="footer-p-tag footer-p-tag-font">
                         Bengaluru:
                     </p>
                     <p class="footer-p-tag">4th Floor, Salarpuria Towers,No 22, Hosur Main road ,
                     </p>
                     <p class="footer-p-tag">6th Block, Koramangala, Bengaluru - 560034</p>
                     <p class="footer-p-tag">Phone: 09742087612</p>                     
                 </div>

                 <div class="col-xs-12 col-md-4">
                     <p class="footer-p-tag footer-p-tag-font">
                         Bengaluru:
                     </p>
                     <p class="footer-p-tag">4th Floor, Salarpuria Towers,No 22, Hosur Main road ,
                     </p>
                     <p class="footer-p-tag">6th Block, Koramangala, Bengaluru - 560034</p>
                     <p class="footer-p-tag">Phone: 09742087612</p>                     
                 </div>
            </div>
            <div class="container">
                <div class="col-md-12 col-xs-12 col-lg-12">
                    <p class="footer-p-tag footer-p-tag-font" style="  padding-top: 30px;  font-weight: 600 !important;">Around the Web</p>                    
                </div>
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <ul class="socialMediaIcons">
                        <li class="facebookIcon socialLinks">
                            <a href="https://www.facebook.com/stones2milestones/">
                                <div class="fb"></div>
                            </a>
                        </li>
                        <li class="linkedIn socialLinks">
                            <a href="https://www.linkedin.com/company-beta/1974028/">
                                <div class="linkedin"></div>
                            </a>
                        </li>
                        <li class="blog socialLinks">
                            <a href="http://stones2milestones.com/blog">
                                <div class="blog"></div>
                            </a>
                        </li>
                        <li class="twitter socialLinks">
                            <a href="https://twitter.com/S2MTweets">
                                <div class="twitter"></div>
                            </a>
                        </li>
                        <li class="google socialLinks">
                            <a href="#">
                                <div class="googleplus"></div>
                            </a>
                        </li> 
                    </ul>
                </div>
            </div>
        </div> -->
        <div class="footer-below-two">
            <div class="container">
                <div class="col-lg-4 col-md-4 col-xs-12">
                    <p class="footer-p-tag allrightreserved">From 2008 Stones2Milestones All Rights Reserved</p>
                </div>
                <!-- <div class="col-md-6 col-lg-6 col-xs-12">
                    <p class="footer-p-tag">Privacy Policy | Terms of Service | Security & Compliance | Sitemap </p>
                </div> -->
            </div>
        </div>
        <div class="footertagline">
            <div class="col-md-12 col-lg-12 col-xs-12 footertagline-div">
                <img class="txt_img" src="img/worded-with-care.svg" alt="">
            </div>
        </div>
    </footer>
    
    

    <div class="modal fade" id="myModalthankyou" role="dialog" style="padding-right: 0px;">
                          <div class="modal-dialog">
                          
                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">×</button>
                                
                              </div>
                              <div class="modal-body">
                                 <h1 class="thankyou">Thank you! We shall get in touch shortly.</h1>
                              </div>
                            </div>
                            
                          </div>
      </div>  
    <!-- Plugin JavaScript -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="https://cdn.firebase.com/js/client/2.2.0/firebase.js"></script> 
    <!-- <script src="js/gen_validatorv4.js" type="text/javascript"></script> -->

    <!-- <script type="text/javascript">
        $(document).ready(function(){
            $('.fadeInUpdiv').addClass('fadeInUp');    
        });
        
    </script> -->
    <!-- <script  type="text/javascript">
var frmvalidator = new Validator("newActivity");
frmvalidator.addValidation("name","req","Please enter your First Name");
frmvalidator.addValidation("name","maxlen=20",
"Max length for FirstName is 20");
frmvalidator.addValidation("email","maxlen=50");
frmvalidator.addValidation("email","req");
frmvalidator.addValidation("email","email");
frmvalidator.addValidation("phone","maxlen=50");
frmvalidator.addValidation("phone","numeric");
</script> -->
    <script type="text/javascript">
        $('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 4000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});
        
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
  var owl = $('#owl2');
  owl.owlCarousel({
    items:4,
    loop:true,
    margin:10,
    autoplay:true,
    
    autoplayHoverPause:false,
    autoplayHoverPause:false,
        responsive:{
        0:{
            items:2
        },
        600:{
            items:3
        },
        1000:{
            items:4
        },
        1300:{
            items:4
        }
    }
});
});
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
  var owl = $('#owl1');
  owl.owlCarousel({
    items:3,
    loop:true,
    margin:10,
    autoplay:true,
    
    autoplayHoverPause:false,
        responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:3
        },
        1300:{
            items:3
        }
    }
});
});
    </script>
    <script type="text/javascript">
        $(document).ready(function(){

    var native_width = 0;
    var native_height = 0;
  $(".large").css("background","url('" + $(".small").attr("src") + "') no-repeat");

    //Now the mousemove function
    $(".magnify").mousemove(function(e){
        //When the user hovers on the image, the script will first calculate
        //the native dimensions if they don't exist. Only after the native dimensions
        //are available, the script will show the zoomed version.
        if(!native_width && !native_height)
        {
            //This will create a new image object with the same image as that in .small
            //We cannot directly get the dimensions from .small because of the 
            //width specified to 200px in the html. To get the actual dimensions we have
            //created this image object.
            var image_object = new Image();
            image_object.src = $(".small").attr("src");
            
            //This code is wrapped in the .load function which is important.
            //width and height of the object would return 0 if accessed before 
            //the image gets loaded.
            native_width = image_object.width;
            native_height = image_object.height;
        }
        else
        {
            //x/y coordinates of the mouse
            //This is the position of .magnify with respect to the document.
            var magnify_offset = $(this).offset();
            //We will deduct the positions of .magnify from the mouse positions with
            //respect to the document to get the mouse positions with respect to the 
            //container(.magnify)
            var mx = e.pageX - magnify_offset.left;
            var my = e.pageY - magnify_offset.top;
            
            //Finally the code to fade out the glass if the mouse is outside the container
            if(mx < $(this).width() && my < $(this).height() && mx > 0 && my > 0)
            {
                $(".large").fadeIn(100);
            }
            else
            {
                $(".large").fadeOut(100);
            }
            if($(".large").is(":visible"))
            {
                //The background position of .large will be changed according to the position
                //of the mouse over the .small image. So we will get the ratio of the pixel
                //under the mouse pointer with respect to the image and use that to position the 
                //large image inside the magnifying glass
                var rx = Math.round(mx/$(".small").width()*native_width - $(".large").width()/2)*-1;
                var ry = Math.round(my/$(".small").height()*native_height - $(".large").height()/2)*-1;
                var bgp = rx + "px " + ry + "px";
                
                //Time to move the magnifying glass with the mouse
                var px = mx - $(".large").width()/2;
                var py = my - $(".large").height()/2;
                //Now the glass moves with the mouse
                //The logic is to deduct half of the glass's width and height from the 
                //mouse coordinates to place it with its center at the mouse coordinates
                
                //If you hover on the image now, you should see the magnifying glass in action
                $(".large").css({left: px, top: py, backgroundPosition: bgp});
            }
        }
    })
});
    </script>
    <script type="text/javascript">
        var tabCarousel = setInterval(function() {
    var tabs = $('#yourTabWrapper .nav-tabs > li'),
        active = tabs.filter('.active'),
        next = active.next('li'),
        toClick = next.length ? next.find('a') : tabs.eq(0).find('a');

    toClick.trigger('click');
}, 20000);
    </script>
    <script type="text/javascript">
        $('#myModal1').on('shown.bs.modal', function () {
  $('#myInput').focus()
})
    </script>
    <!-- <script type="text/javascript">
      $(document).ready(function(){
        $('#myCarousel').carousel({
            interval: 4000,
            cylce: true
        })
        $('.carousel .item').each(function () {
            var next = $(this).next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }
            next.children(':first-child').clone().appendTo($(this));
            if (next.next().length > 0) {
                next.next().children(':first-child').clone().appendTo($(this));
            } else {
                $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
            }
        });
    });

    </script>
     --><!-- <script src="js/jquery.bxslider.min.js"></script> -->
     <!-- jQuery -->
      

    <!-- Bootstrap Core JavaScript -->
       <script src="lib/bootstrap/js/bootstrap.min.js"></script>
       <script type="text/javascript" src="js/YouTubePopUp.jquery.js"></script>
    <!-- Latest compiled and minified JavaScript -->
   <!--  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
 -->
    
    <!-- <script src="js/jquery.elevatezoom.js"></script> -->
    <!-- Theme JavaScript -->
     
     <script src="js/owl.carousel.min.js"></script>
     
     <script src="js/new-age.min.js"></script>
     <script type="text/javascript">
         $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
jQuery(function(){
          jQuery("a.demo").YouTubePopUp().click();
          // jQuery("a.demo").YouTubePopUp({ autoplay: 0 }); // Disable autoplay
      });
     </script>
    
<!-- Start of Async Drift Code -->
<script>
!function() {
  var t;
  if (t = window.driftt = window.drift = window.driftt || [], !t.init) return t.invoked ? void (window.console && console.error && console.error("Drift snippet included twice.")) : (t.invoked = !0, 
  t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ], 
  t.factory = function(e) {
    return function() {
      var n;
      return n = Array.prototype.slice.call(arguments), n.unshift(e), t.push(n), t;
    };
  }, t.methods.forEach(function(e) {
    t[e] = t.factory(e);
  }), t.load = function(t) {
    var e, n, o, i;
    e = 3e5, i = Math.ceil(new Date() / e) * e, o = document.createElement("script"), 
    o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + i + "/" + t + ".js", 
    n = document.getElementsByTagName("script")[0], n.parentNode.insertBefore(o, n);
  });
}();
drift.SNIPPET_VERSION = '0.3.1';
drift.load('z5525s5rhhu9');
</script>

<!-- End of Async Drift Code -->
<!-- <script src="https://www.gstatic.com/firebasejs/4.0.0/firebase.js"></script> -->
<script type="text/javascript">
var dbRef = new Firebase("https://s2m-database.firebaseio.com/");

var contactsRef = dbRef.child('contacts')

//save contact

$('.addValue').on("click", function( event ) {  

    event.preventDefault();

    if( $('#formName').val() == '' || $('#formEmail').val() == '' || $('#formPhone').val() == ''  ){
        alert('Please Enter Name, Mobile Number and Email!');
    }
    else {
      console.log($('#name').val() )    
      contactsRef

        .push({

          name: $('#formName').val(),

          email: $('#formEmail').val(),

         

            formPhone: $('#formPhone').val(),

            formText: $('#formText').val(),

            selectParentTeacher: $('#selectParentTeacher').val()

          

        })

        //contactForm.reset();
        $('#myModal').modal('toggle');
        $('#myModalthankyou').modal('toggle');

    }

  });

      
</script>

<script type="text/javascript">
  var dbRef = new Firebase("https://s2m-database.firebaseio.com/");

var emailaddress = dbRef.child('signups')

$('.submitsignup').on("click", function( event ) {

 event.preventDefault();

    if( $('#email').val() != ''  ){

      emailaddress

        .push({

          email: $('#MERGE0').val(),

        })

        contactForm.reset();

    } else {

      alert('Please Enter Name, Mobile Number and Email!');

    }


});
</script>
<script type="text/javascript">
    $(window).on("scroll", function () {
    if ($(this).scrollTop() > 70) {
        $(".exp-us").addClass("color-change");
        $(".p-tag-exp").addClass("color-change-p");

        $('#phone-logo img').attr('src', 'img/phone-call.svg');

    }
    else {
        $(".exp-us").removeClass("color-change");
        $(".p-tag-exp").removeClass("color-change-p");
        $('#phone-logo img').attr('src', 'img/phonecall.svg');
    }
});
</script>
 <script> (function() { var qs,js,q,s,d=document, gi=d.getElementById, ce=d.createElement, gt=d.getElementsByTagName, id="typef_orm_share", b="https://embed.typeform.com/"; if(!gi.call(d,id)){ js=ce.call(d,"script"); js.id=id; js.src=b+"embed.js"; q=gt.call(d,"script")[0]; q.parentNode.insertBefore(js,q) } })() </script>
<script type="text/javascript"></script>
</body>

</html>
