<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Stones2Milestones is an organisation in the education space that aims to impact the way in which school going children between the ages of 3 to class 3 learn to read in English.">
    <meta name="author" content="">
    <meta name="keywords" content="Reading,children, grade 1 English, grade 2 English, English, child,Novel, learn, learn English, learn reading,stones, stones2milestones, reader, English grammar, language, english Novel, blog , about , about s2m, s2m , wingsofwords, getfreadom, wowconnect , wow">
    <title>Stones2Milestones| Policy</title>
    <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="img/s2sfavicon.png"/>
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/main.css">   
    <link rel="stylesheet" href="css/modal.css">   
    <link href="css/new-age.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/owl.theme.default.min.css" rel="stylesheet">
    <link href="css/footer.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/YouTubePopUp.css">
    <script src="https://cdn.firebase.com/js/client/2.2.0/firebase.js"></script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-81416339-1', 'auto');
      ga('send', 'pageview');

    </script> 
</head>
<style type="text/css">
.typeform-popup-wrapper.typeform-popup-mode-popup{
        width: 50%!important;
    height: 80%!important;
    top: 10%!important;
    left: 25%!important;
      }
      .header-overlay{
        background: rgba(0,0,0,0.9)
      }
</style>
<body id="page-top" data-spy="scroll" data-target=".nav-pills" data-offset="50">
<!-- Nav Bar starts -->
     <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span><i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand " href="http://www.stones2milestones.com/">
                    <img src="img/s2s.png" style="width: 150px;">
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                      <li class="dropdown list-view">
                        <a href="http://www.stones2milestones.com/about.php" class="dropdown-toggle page-scroll" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <p>about</p></a>
                        <ul class="dropdown-menu">
                          <li><a href="http://www.stones2milestones.com/about.php">About</a></li>
                          <li><a href="http://www.stones2milestones.com/about.php#visions2m">Vision</a></li>
                          <li><a href="http://www.stones2milestones.com/about.php#missions2m">Mission</a></li>
                          <li><a href="http://www.stones2milestones.com/about.php#corevaluess2m">Core Values</a></li>
                          <li><a href="http://www.stones2milestones.com/about.php#teams2m">Team</a></li>
                        </ul>
                      </li>
                      <li class="dropdown list-view">
                        <a href="#" class="dropdown-toggle page-scroll" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <p>For Schools</p></a>
                        <ul class="dropdown-menu">
                          <li><a href="http://www.wingsofwords.com/">Wings of Words</a></li>
                          <li><a href="https://play.google.com/store/apps/details?id=com.wowconnect">WOWConnect</a></li>
                          <li><a href="http://www.wingsofwords.com/blog">Blog</a></li>
                        </ul>
                      </li>
                      <li class="dropdown list-view">
                        <a href="#" class="dropdown-toggle page-scroll" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <p>For Parents</p></a>
                        <ul class="dropdown-menu">
                          <li><a href="https://www.getfreadom.com/">Freadom</a></li>
                          <li><a href="https://play.google.com/store/apps/details?id=com.application.freadom&hl=en">Download App</a></li>
                          <li><a href="https://getfreadom.com/blog/">Blog</a></li>
                        </ul>
                      </li>
                      <li class="list-view mobile-view">
                        <a class="page-scroll" href="http://www.stones2milestones.com/about.php">
                            <p>About</p>
                        </a>
                      </li>
                      <li class="list-view mobile-view">
                        <a class="page-scroll" href="http://www.stones2milestones.com/partners.php">
                            <p>Partner With Us</p>
                        </a>
                      </li><li class="list-view mobile-view">
                        <a class="page-scroll" href="http://www.wingsofwords.com/">
                            <p>Wings of words</p>
                        </a>
                      </li><li class="list-view mobile-view"> 
                        <a class="page-scroll" href="https://play.google.com/store/apps/details?id=com.wowconnect">
                            <p>WOWConnect</p>
                        </a>
                      </li><li class="list-view mobile-view">
                        <a class="page-scroll" href="https://www.getfreadom.com/">
                            <p>Freadom</p>
                        </a>
                      </li><li class="list-view mobile-view" >
                        <a class="page-scroll" href="https://play.google.com/store/apps/details?id=com.application.freadom&hl=en">
                            <p>Download the App Freadom</p>
                        </a>
                      </li>
                      <li class="list-view">
                        <a class="page-scroll" href="http://www.stones2milestones.com/blog/">
                            <p>Blog</p>
                        </a>
                      </li>
                     <li class="dropdown list-view">
                        <a href="http://www.stones2milestones.com/partners.php" class="dropdown-toggle page-scroll" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <p>Partner With Us</p></a>
                        <ul class="dropdown-menu">
                          <li><a href="http://www.stones2milestones.com//partners.php">Partners</a></li>
                          <li><a href="http://www.stones2milestones.com//partners.php#engages2m">How Do We Enagage?</a></li>
                          <li><a href="http://www.stones2milestones.com//partners.php#partnerss2m">Partner Schools</a></li>
                          <li><a href="http://www.stones2milestones.com//partners.php#csrs2m">Social Impact</a></li>
                          <li><a href="http://www.stones2milestones.com//partners.php#testimonials2m">Testimonial</a></li>
                        </ul>
                      </li>
                      <li class="exp-us list-view">
                        <a class="typeform-share button" href="https://stones2milestones.typeform.com/to/ksK99C" data-mode="popup" style="padding: 6px;" target="_blank"><p class="p-tag-exp" style="margin-bottom: 0px;
    text-transform: uppercase;    font-size: 10px;">Join the mission</p></a>
                    </li>
                    <!-- <li>
                      <button type="button" class="btn btn-info btn-lg" >Open Modal</button>
                    </li> -->
                    <!-- Modal -->

                        
                    <li class="phone">
                        <span id="phone-logo"><img src="img/phonecall.svg" style="margin-right:5px; width: 20px;">+91 90770 77777</span>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
<div class="modal fade" id="myModal" role="dialog">
                          <div class="modal-dialog">
                          
                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">×</button>
                                <h4 class="modal-title">Join The Mission</h4>
                              </div>
                              <div class="modal-body">
                              <div role="form" class="wpcf7" id="wpcf7-f433-o1" lang="en-US" dir="ltr">
                      <div class="screen-reader-response"></div>
                      <form action="" method="post" id= "newActivity" name="newActivity" class="" novalidate="novalidate">
                      <div style="display: none;">
                      <input type="hidden" name="_wpcf7" value="433">
                      <input type="hidden" name="_wpcf7_version" value="4.5.1">
                      <input type="hidden" name="_wpcf7_locale" value="en_US">
                      <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f433-o1">
                      <input type="hidden" name="_wpnonce" value="37e90b15f0">
                      </div>
                      <p><span class="wpcf7-form-control-wrap iam"><select id="selectParentTeacher" name="iam" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required pop-in" aria-required="true" aria-invalid="false"><option value="I am a Parent">I am a Parent</option><option value="I am a Teacher">I am a Teacher</option><option value="I am a School Admin">I am a School Admin</option></select></span><br>
                      <span class="wpcf7-form-control-wrap name">
                      <input type="text" id="formName" name="name" required  value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required pop-in" aria-required="true" aria-invalid="false" placeholder="Your name"></span><br>
                      <span class="wpcf7-form-control-wrap phone">
                      <input type="tel" name="phone" id="formPhone" required value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel pop-in" aria-required="true" aria-invalid="false" placeholder="Mobile number"></span><br>
                      <span class="wpcf7-form-control-wrap email">
                      <input type="email" id="formEmail" name="email" value="" required size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email pop-in" aria-required="true" aria-invalid="false" placeholder="Email address"></span><br>
                      <span class="wpcf7-form-control-wrap school">
                      <input type="text" id="formText"name="school" value="" size="40" class="wpcf7-form-control wpcf7-text pop-in" aria-invalid="false" placeholder="School name (if you are a Teacher or School Admin)"></span><br>
                      <input type="submit" id="saveFrom" value="Request" class="wpcf7-form-control wpcf7-submit popbtn addValue"><img class="ajax-loader" src="" alt="Sending ..." style="visibility: hidden;"></p>
                      <div class="wpcf7-response-output wpcf7-display-none"></div></form></div>        </div>
                            </div>
                            
                          </div>
</div>

    <header class="header-about">
      <div class="header-overlay">
          <div class="container" style="">
          <div class="header-about-tag">
           Terms and Policy 
          </div>
         </div>    
      </div>
    </header>
    <section id="home" class="home bg-primary text-center parallax">
        <div class="container text-center mobile-bg-about">
          <div class="header-overlay">
            <div class="col-xs-12 ">
              <div class="col-xs-12">
                <h1 class="heading-tag-mobile">Terms and Policy</h1>
              </div>
              <div class="col-xs-12 viedo-bg" >
                <!-- <a href="https://www.youtube.com/watch?v=ygwRc9WuBBM" class="demo" data-toggle="tooltip" title="" data-original-title="Personal Message From The Founder!">
                    <img src="img/playsvgmedia.svg" style="    width: 40px;padding-bottom: 20px;"> -->
              </div>
            </div>
          </div>          
        </div>
        <div class="container" >
          <div class="col-xs-12 col-md-12 col-lg-12">
            <div class="heading">
              
              <div class="col-sm-12 col-md-12 col-lg-12 sub-heading" >
                     <h3 style="text-align: left;">Terms</h3>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12 content-div">
                     <p style="text-align: left;" >By accessing the website at <a href="http://www.stones2milestones.com/">http://www.stones2milestones.com/</a>, you are agreeing to be bound by these terms of service, all applicable laws and regulations, and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you are prohibited from using or accessing this site. The materials contained in this website are protected by applicable copyright and trademark law.</p>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12 sub-heading" >
                     <h3 style="text-align: left;">Use License</h3>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12 content-div">
                     <p style="text-align: left;" >a. Permission is granted to temporarily download one copy of the materials (information or software) on Stones2Milestones Edu Services Pvt. Ltd.'s website for personal, non-commercial transitory viewing only. This is the grant of a license, not a transfer of title, and under this license you may not: <br><br>
                      1.modify or copy the materials;<br>
                      2.use the materials for any commercial purpose, or for any public display (commercial or non-commercial);<br>
                      3.attempt to decompile or reverse engineer any software contained on Stones2Milestones Edu Servicevices Pvt. Ltd.'s website;<br>
                      4.remove any copyright or other proprietary notations from the materials; <br>
                      5.transfer the materials to another person or "mirror" the materials on any other server.<br>
                      <br>
                      b. This license shall automatically terminate if you violate any of these restrictions and may be terminated by Stones2Milestones Edu Services Pvt. Ltd. at any time. Upon terminating your viewing of these materials or upon the termination of this license, you must destroy any downloaded materials in your possession whether in electronic or printed format.</p>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12 sub-heading" >
                     <h3 style="text-align: left;">Disclaimer</h3>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12 content-div">
                     <p style="text-align: left;" >a. The materials on Stones2Milestones Edu Services Pvt. Ltd.'s website are provided on an 'as is' basis. Stones2Milestones Edu Services Pvt. Ltd. makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties including, without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights.<br>
                      b. Further, Stones2Milestones Edu Services Pvt. Ltd. does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its website or otherwise relating to such materials or on any sites linked to this site.</p>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12 sub-heading" >
                     <h3 style="text-align: left;">Limitations</h3>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12 content-div">
                     <p style="text-align: left;" >In no event shall Stones2Milestones Edu Services Pvt. Ltd. or its suppliers be liable for any damages (including, without limitation, damages for loss of data or profit, or due to business interruption) arising out of the use or inability to use the materials on Stones2Milestones Edu Services Pvt. Ltd.'s website, even if Stones2Milestones Edu Services Pvt. Ltd. or a Stones2Milestones Edu Services Pvt. Ltd. authorized representative has been notified orally or in writing of the possibility of such damage. Because some jurisdictions do not allow limitations on implied warranties, or limitations of liability for consequential or incidental damages, these limitations may not apply to you..</p>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12 sub-heading" >
                     <h3 style="text-align: left;">Accuracy of materials</h3>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12 content-div">
                     <p style="text-align: left;" >The materials appearing on Stones2Milestones Edu Services Pvt. Ltd.'s website could include technical, typographical, or photographic errors. Stones2Milestones Edu Services Pvt. Ltd. does not warrant that any of the materials on its website are accurate, complete or current. Stones2Milestones Edu Services Pvt. Ltd. may make changes to the materials contained on its website at any time without notice. However Stones2Milestones Edu Services Pvt. Ltd. does not make any commitment to update the materials.</p>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12 sub-heading" >
                     <h3 style="text-align: left;">Links</h3>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12 content-div">
                     <p style="text-align: left;" >Stones2Milestones Edu Services Pvt. Ltd. has not reviewed all of the sites linked to its website and is not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement by Stones2Milestones Edu Services Pvt. Ltd. of the site. Use of any such linked website is at the user's own risk.</p>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12 sub-heading" >
                     <h3 style="text-align: left;">Modifications</h3>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12 content-div">
                     <p style="text-align: left;" >Stones2Milestones Edu Services Pvt. Ltd. may revise these terms of service for its website at any time without notice. By using this website you are agreeing to be bound by the then current version of these terms of service.</p>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12 sub-heading" >
                     <h3 style="text-align: left;">Governing Law</h3>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12 content-div">
                     <p style="text-align: left;" >These terms and conditions are governed by and construed in accordance with the laws of Haryana and you irrevocably submit to the exclusive jurisdiction of the courts in that State or location.</p>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-12 sub-heading" >
                     <h3 style="text-align: left;">Policy</h3>
              </div>              
              <div class="col-sm-12 col-md-12 col-lg-12 content-div">
                     <p style="text-align: left;" >Your privacy is important to us. <br>It is Stones2Milestones Edu Services Pvt. Ltd.'s policy to respect your privacy regarding any information we may collect while operating our website. Accordingly, we have developed this privacy policy in order for you to understand how we collect, use, communicate, disclose and otherwise make use of personal information. We have outlined our privacy policy below. <br><br>
                     We will collect personal information by lawful and fair means and, where appropriate, with the knowledge or consent of the individual concerned.<br>
Before or at the time of collecting personal information, we will identify the purposes for which information is being collected.<br>
We will collect and use personal information solely for fulfilling those purposes specified by us and for other ancillary purposes, unless we obtain the consent of the individual concerned or as required by law.<br>
Personal data should be relevant to the purposes for which it is to be used, and, to the extent necessary for those purposes, should be accurate, complete, and up-to-date.<br>
We will protect personal information by using reasonable security safeguards against loss or theft, as well as unauthorized access, disclosure, copying, use or modification.<br>
We will make readily available to customers information about our policies and practices relating to the management of personal information.<br>
We will only retain personal information for as long as necessary for the fulfilment of those purposes.<br><br>
We are committed to conducting our business in accordance with these principles in order to ensure that the confidentiality of personal information is protected and maintained. Stones2Milestones Edu Services Pvt. Ltd. may change this privacy policy from time to time at Stones2Milestones Edu Services Pvt. Ltd.'s sole discretion.
                     </p>
              </div>



            </div>
          </div>
        </div>
        <section class="success fr_sec yellow newsletter_sec">
        <div class="container">
            <div class="row">
                <div class="newsletter_holder col-xs-12">
                    <div class="newsletter_heading col-xs-12">
                        <h4 class="newsletter_title">Sign up to receive amazing stuff around reading</h4>
                        <!-- <h5 class="newsletter_subtitle">Delivered weekly straight into your inbox</h5> -->
                    </div>
                    <div class="newsletter_form col-xs-12 col-xs-offset-0 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                        <form action="http://sendy.stones2milestones.com/subscribe" method="POST" accept-charset="utf-8" class="flex_holder" style=" border: 2px solid #0e6837;box-shadow: 0px -1px 10px -3px;">
                            <!-- <input type="hidden" name="u" value="3eaaed45454f13a1c2c555c5a"> -->
                            <input type="hidden" name="list" value="joNJFfKW2v484XtmxrUEtA"/>
                            <!-- <input type="hidden" name="id" value="cc047a2e71"> -->
                            <div class="flex_elements input_with_icon">
                            <i class="fa fa-paper-plane" aria-hidden="true"></i>
                                <input type="email" autocapitalize="off" autocorrect="off" name="MERGE0" id="MERGE0" size="25" value="" class="inputtext" placeholder="Enter your Email">
                            </div>
                            
                            <a href="#contactModalsign" role="button"  data-toggle="modal">
                            <input type="submit"  name="submit" id="submit" class="flex_elements btn btn-primary submitsignup" value="Sign Up">
                            </a>
                            <div id="contactModalsign" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <!-- <h3 id="myModalLabel">Almost finished...</h3> -->
                                  </div>
                                  <div class="modal-body">
                                   <div class="s2mblack" style="padding: 20px;">
                                       <img src="img/s2mblack.png">
                                   </div>
                                   <div style=" padding: 30px;text-align:center; margin-top: -80px;">
                                       <p>Thank you for subscribing!</p>
                                   </div>
                                  </div>
                                  <!-- <div class="modal-footer" style="border: none;">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true" style="float: left; margin-left: 44%;">Cancel</button>
                                  </div> -->
                                </div>
                              </div>
                            </div>   
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </section>

       <div class="container" style="padding-top: 60px; padding-bottom: 40px; ">
            <!-- Set up your HTML -->
            <div class="owl-carousel owl2-style" id="owl2">
                
                
                <a href="#"><img src="img/youwecan.png" alt="logo05" title="logo05" style="width:150px!important;"></a>
                <a href="#"><img src="img/orchid.png" alt="logo05" title="logo05"></a>
                <a href="#"><img src="img/logo02-2.jpg" alt="logo05" title="logo05"></a>
                <a href="#"><img src="img/logo06-1.jpg" alt="logo05" title="logo05"></a>
                <a href="#"><img src="img/manyavar.png" alt="logo05" title="manyavar" style="width: 150px!important;"></a>
                <a href="#"><img src="img/inclusive-india-logo.png" alt="blackstone-logo" title="blackstone-logo" style="width:150px!important;padding-top: 10px;"></a>
                <a href="#"><img src="img/logo03-1.jpg" alt="logo05" title="logo05" style="width:150px!important;padding-top: 5px;"></a>   
                <a href="#"><img src="img/teach4india.png" alt="logo05" title="logo05" style="width: 180px!important; padding-top: 30px;"></a>                        
            </div>
        </div>

    <section class="school-count">
         <div class="container">
            <div class="col-md-12 col-sm-12 col-lg-12 ">
                <div class="col-xs-4 col-md-4 col-lg-4 school-count-div">
                <div class="col-xs-6 col-md-6 col-lg-6">
                    <img src="img/01png.png" class="school-count-width">
                </div>
                <div class="col-md-6 col-lg-6 col-xs-6 school-count-tag">
                <div class="row count">
                    120
                </div>
                <div class="row">
                    <h3 class="school-tag">Schools</h3>
                </div>
                </div>
                </div>
                <div class="col-xs-4 col-md-4 col-lg-4 school-count-div">
                <div class="col-xs-6 col-md-6 col-lg-6">
                    <img src="img/02png.png" class="school-count-width">
                </div>
                <div class="col-md-6 col-lg-6 col-xs-6 school-count-tag">
                <div class="row count">
                700
                </div>
                <div class="row">
                    <h3 class="school-tag">Teachers</h3>
                </div>
                </div>
                </div>
                <div class="col-xs-4 col-md-4 col-lg-4 school-count-div">
                <div class="col-xs-6 col-md-6 col-lg-6">
                    <img src="img/03png.png" class="school-count-width">
                </div>
                <div class="col-md-6 col-lg-6 col-xs-6 school-count-tag">
                <div class="row count">
                   30000
                </div>
                <div class="row">
                    <h3 class="school-tag">Students</h3>
                </div>
                </div>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <div class="footer-above">
            <div class="container">
                <div class="col-xs-12 col-md-4 col-lg-4">
                    <a href="http://www.stones2milestones.com/"><img class="img-responsive" src="img/s2s.png" style="width: 200px;padding-bottom: 10px;"></a>
                    <p class="footer-p-tag">
                        Creating a nation of 10 million readers by 2022                     
                    </p>
                     <p class="footer-p-tag">                        
                        We are an organisation in the education space <br> that aims to impact the way in which children <br> learn to read in English.
                    </p>
                    <div class="col-lg-12 col-md-12 col-xs-12" style="padding-left: 0px;">
                    <ul class="socialMediaIcons">
                        <li class="facebookIcon socialLinks">
                            <a href="https://www.facebook.com/stones2milestones/">
                                <div class="fb"></div>
                            </a>
                        </li>
                        <li class="linkedIn socialLinks">
                            <a href="https://www.linkedin.com/company-beta/1974028/">
                                <div class="linkedin"></div>
                            </a>
                        </li>
                        <li class="blog socialLinks">
                            <a href="http://stones2milestones.com/blog">
                                <div class="blog"></div>
                            </a>
                        </li>
                        <li class="twitter socialLinks">
                            <a href="https://twitter.com/S2MTweets">
                                <div class="twitter"></div>
                            </a>
                        </li>
                        <!-- <li class="google socialLinks">
                            <a href="#">
                                <div class="googleplus"></div>
                            </a>
                        </li>  -->
                    </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-md-2 col-lg-2 ">
                    
                   
                        <p class="footer-p-tag-head">Company</p>
                        <p class="footer-p-tag"><a href="http://www.stones2milestones.com/about.php">About</a>  </p>
                        <p class="footer-p-tag"><a href="mailto:work@stones2milestones.com" > Careers</a> </p>
                        <p class="footer-p-tag"><a href="http://www.stones2milestones.com/partners.php">Partner With Us</a></p>
                        <!-- <p class="footer-p-tag"><a href="mailto:work@stones2milestones.com" >Contact Us</a></p> -->
                        <p class="footer-p-tag"><a href="http://www.stones2milestones.com/blog">Blog</a></p>
                        <p class="footer-p-tag"><a href="http://www.stones2milestones.com/policy.php">Policy</a></p>
                   
                </div>
                <div class="col-xs-12 col-md-2 col-lg-2 ">
                   
                        <p class="footer-p-tag-head">For Schools</p>
                        <p class="footer-p-tag"><a href="http://www.wingsofwords.com/">Wings of Words</a></p>
                        <p class="footer-p-tag"><a href="https://play.google.com/store/apps/details?id=com.wowconnect">WOW Connect</a></p>
                        <p class="footer-p-tag"><a href="http://www.wingsofwords.com/blog">Blog</a></p>
                        <p class="footer-p-tag"><a href="https://play.google.com/store/apps/details?id=com.wowconnect"><img src="img/googleplay.png" class="footer-google"></a></p>

                        
                   
                </div>
                <div class="col-xs-12 col-md-2 col-lg-2 ">
                   
                        <p class="footer-p-tag-head">For Parents</p>
                        <p class="footer-p-tag"><a href="http://www.getfreadom.com/">Freadom</a></p>
                        <p class="footer-p-tag"><a href="https://play.google.com/store/apps/details?id=com.application.freadom&hl=en">Download App</a></p>
                        <p class="footer-p-tag"><a href="http://www.getfreadom.com/blog">Blog</a></p>
                        <p class="footer-p-tag"><a href="https://play.google.com/store/apps/details?id=com.application.freadom&hl=en"><img src="img/googleplay.png" class="footer-google"></a></p>
                        
                   
                    <!-- <div class="col-xs-12">
                        <p class="footer-p-tag-head">CONTACT</p>
                        <p class="footer-p-tag">support@stones2milestones.com</p>
                        <p class="footer-p-tag ">Gurgaon </p>
                        <p class="footer-p-tag">419, Tower A, Spaze ITech Park, Sohna Road, Sector 49, Gurgaon - 122018, Haryana</p>
                        <p class="footer-p-tag">Phone: +91 40 4137 1111 </p>
                    </div> -->
                </div>
                <div class="col-xs-12 col-md-2 col-lg-2 ">
                   
                        <p class="footer-p-tag-head">Location</p>
                        <p class="footer-p-tag ">
                         Gurgaon:
                      </p>
                     <p class="footer-p-tag">419, Tower A, Spaze ITech Park, Sohna Road, <br>Sector 49, Gurgaon - 122018, Haryana
                     </p>
                     <!-- <p class="footer-p-tag"></p> -->
                     <p class="footer-p-tag">Phone: +91 90770 77777</p>      
                   
                </div>
            </div>
        </div>
        <!-- <div class="footer-below">
            <div class="container">
                <div class="col-md-12 col-xs-12 col-lg-12">
                    <p class="footer-p-tag-head footer-p-tag-font">Locations</p>
                </div>
                 <div class="col-xs-12 col-md-4">
                     <p class="footer-p-tag footer-p-tag-font">
                         Gurgaon:
                     </p>
                     <p class="footer-p-tag">419, Tower A, Spaze ITech Park, Sohna Road, 
                     </p>
                     <p class="footer-p-tag">Sector 49, Gurgaon - 122018, Haryana</p>
                     <p class="footer-p-tag">Phone: 09742087612</p>                     
                 </div>

                 <div class="col-xs-12 col-md-4">
                     <p class="footer-p-tag footer-p-tag-font">
                         Bengaluru:
                     </p>
                     <p class="footer-p-tag">4th Floor, Salarpuria Towers,No 22, Hosur Main road ,
                     </p>
                     <p class="footer-p-tag">6th Block, Koramangala, Bengaluru - 560034</p>
                     <p class="footer-p-tag">Phone: 09742087612</p>                     
                 </div>

                 <div class="col-xs-12 col-md-4">
                     <p class="footer-p-tag footer-p-tag-font">
                         Bengaluru:
                     </p>
                     <p class="footer-p-tag">4th Floor, Salarpuria Towers,No 22, Hosur Main road ,
                     </p>
                     <p class="footer-p-tag">6th Block, Koramangala, Bengaluru - 560034</p>
                     <p class="footer-p-tag">Phone: 09742087612</p>                     
                 </div>

                 <div class="col-xs-12 col-md-4">
                     <p class="footer-p-tag footer-p-tag-font">
                         Bengaluru:
                     </p>
                     <p class="footer-p-tag">4th Floor, Salarpuria Towers,No 22, Hosur Main road ,
                     </p>
                     <p class="footer-p-tag">6th Block, Koramangala, Bengaluru - 560034</p>
                     <p class="footer-p-tag">Phone: 09742087612</p>                     
                 </div>

                 <div class="col-xs-12 col-md-4">
                     <p class="footer-p-tag footer-p-tag-font">
                         Bengaluru:
                     </p>
                     <p class="footer-p-tag">4th Floor, Salarpuria Towers,No 22, Hosur Main road ,
                     </p>
                     <p class="footer-p-tag">6th Block, Koramangala, Bengaluru - 560034</p>
                     <p class="footer-p-tag">Phone: 09742087612</p>                     
                 </div>
            </div>
            <div class="container">
                <div class="col-md-12 col-xs-12 col-lg-12">
                    <p class="footer-p-tag footer-p-tag-font" style="  padding-top: 30px;  font-weight: 600 !important;">Around the Web</p>                    
                </div>
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <ul class="socialMediaIcons">
                        <li class="facebookIcon socialLinks">
                            <a href="https://www.facebook.com/stones2milestones/">
                                <div class="fb"></div>
                            </a>
                        </li>
                        <li class="linkedIn socialLinks">
                            <a href="https://www.linkedin.com/company-beta/1974028/">
                                <div class="linkedin"></div>
                            </a>
                        </li>
                        <li class="blog socialLinks">
                            <a href="http://stones2milestones.com/blog">
                                <div class="blog"></div>
                            </a>
                        </li>
                        <li class="twitter socialLinks">
                            <a href="https://twitter.com/S2MTweets">
                                <div class="twitter"></div>
                            </a>
                        </li>
                        <li class="google socialLinks">
                            <a href="#">
                                <div class="googleplus"></div>
                            </a>
                        </li> 
                    </ul>
                </div>
            </div>
        </div> -->
        <div class="footer-below-two">
            <div class="container">
                <div class="col-lg-4 col-md-4 col-xs-12">
                    <p class="footer-p-tag allrightreserved">From 2008 Stones2Milestones All Rights Reserved</p>
                </div>
                <!-- <div class="col-md-6 col-lg-6 col-xs-12">
                    <p class="footer-p-tag">Privacy Policy | Terms of Service | Security & Compliance | Sitemap </p>
                </div> -->
            </div>
        </div>
        <div class="footertagline">
            <div class="col-md-12 col-lg-12 col-xs-12 footertagline-div">
                <img class="txt_img" src="img/worded-with-care.svg" alt="">
            </div>
        </div>
    </footer>
    
    <div class="modal fade" id="myModalthankyou" role="dialog">
                          <div class="modal-dialog">
                          
                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">×</button>
                                
                              </div>
                              <div class="modal-body">
                                 <h1 class="thankyou">Thank you! We shall get in touch shortly.</h1>
                              </div>
                            </div>
                            
                          </div>
      </div>
    <!-- Plugin JavaScript -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="https://cdn.firebase.com/js/client/2.2.0/firebase.js"></script> 
    

    <!-- <script type="text/javascript">
        $(document).ready(function(){
            $('.fadeInUpdiv').addClass('fadeInUp');    
        });
        
    </script> -->
    <script type="text/javascript">
        $('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 4000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});
        
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
  var owl = $('#owl2');
  owl.owlCarousel({
    items:4,
    loop:true,
    margin:10,
    autoplay:true,
    
    autoplayHoverPause:false,
    autoplayHoverPause:false,
        responsive:{
        0:{
            items:2
        },
        600:{
            items:3
        },
        1000:{
            items:4
        },
        1300:{
            items:4
        }
    }
});
});
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
  var owl = $('#owl1');
  owl.owlCarousel({
    items:3,
    loop:true,
    margin:10,
    autoplay:true,
    
    autoplayHoverPause:false,
        responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:3
        },
        1300:{
            items:3
        }
    }
});
});
    </script>
    <script type="text/javascript">
        var tabCarousel = setInterval(function() {
    var tabs = $('#yourTabWrapper .nav-tabs > li'),
        active = tabs.filter('.active'),
        next = active.next('li'),
        toClick = next.length ? next.find('a') : tabs.eq(0).find('a');

    toClick.trigger('click');
}, 20000);
    </script>
    <script type="text/javascript">
        $('#myModal1').on('shown.bs.modal', function () {
  $('#myInput').focus()
})
    </script>
    <!-- <script type="text/javascript">
      $(document).ready(function(){
        $('#myCarousel').carousel({
            interval: 4000,
            cylce: true
        })
        $('.carousel .item').each(function () {
            var next = $(this).next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }
            next.children(':first-child').clone().appendTo($(this));
            if (next.next().length > 0) {
                next.next().children(':first-child').clone().appendTo($(this));
            } else {
                $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
            }
        });
    });

    </script>
     --><!-- <script src="js/jquery.bxslider.min.js"></script> -->
     <!-- jQuery -->
    <script src="lib/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
       <script src="lib/bootstrap/js/bootstrap.min.js"></script>
       <script type="text/javascript" src="js/YouTubePopUp.jquery.js"></script>
    <!-- Latest compiled and minified JavaScript -->
   <!--  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
 -->
    
    <!-- <script src="js/jquery.elevatezoom.js"></script> -->
    <!-- Theme JavaScript -->
     
     <script src="js/owl.carousel.min.js"></script>
     
     <script src="js/new-age.min.js"></script>
     <!-- <script type="text/javascript">
         $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
jQuery(function(){
          jQuery("a.demo").YouTubePopUp().click();
          // jQuery("a.demo").YouTubePopUp({ autoplay: 0 }); // Disable autoplay
      });
     </script> -->

     <!-- Start of Async Drift Code -->
<script>
!function() {
  var t;
  if (t = window.driftt = window.drift = window.driftt || [], !t.init) return t.invoked ? void (window.console && console.error && console.error("Drift snippet included twice.")) : (t.invoked = !0, 
  t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ], 
  t.factory = function(e) {
    return function() {
      var n;
      return n = Array.prototype.slice.call(arguments), n.unshift(e), t.push(n), t;
    };
  }, t.methods.forEach(function(e) {
    t[e] = t.factory(e);
  }), t.load = function(t) {
    var e, n, o, i;
    e = 3e5, i = Math.ceil(new Date() / e) * e, o = document.createElement("script"), 
    o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + i + "/" + t + ".js", 
    n = document.getElementsByTagName("script")[0], n.parentNode.insertBefore(o, n);
  });
}();
drift.SNIPPET_VERSION = '0.3.1';
drift.load('z5525s5rhhu9');
</script>
<!-- End of Async Drift Code -->

<script type="text/javascript">
var dbRef = new Firebase("https://s2m-database.firebaseio.com/");

var contactsRef = dbRef.child('contacts')

//save contact

$('.addValue').on("click", function( event ) {  

    event.preventDefault();

    if( $('#formName').val() == '' || $('#formEmail').val() == '' || $('#formPhone').val() == ''  ){
        alert('Please Enter Name, Mobile Number and Email!');
    }
    else {
      console.log($('#name').val() )    
      contactsRef

        .push({

          name: $('#formName').val(),

          email: $('#formEmail').val(),

         

            formPhone: $('#formPhone').val(),

            formText: $('#formText').val(),

            selectParentTeacher: $('#selectParentTeacher').val()

          

        })

        //contactForm.reset();
        $('#myModal').modal('toggle');
        $('#myModalthankyou').modal('toggle');

    }

  });

      
</script>

<script type="text/javascript">
  var dbRef = new Firebase("https://s2m-database.firebaseio.com/");

var emailaddress = dbRef.child('signups')

$('.submitsignup').on("click", function( event ) {

 event.preventDefault();

    if( $('#email').val() != ''  ){

      emailaddress

        .push({

          email: $('#MERGE0').val(),

        })

        contactForm.reset();

    } else {

      alert('Please Enter Name, Mobile Number and Email!');

    }


});
</script>
<script type="text/javascript">
    $(window).on("scroll", function () {
    if ($(this).scrollTop() > 70) {
        $(".exp-us").addClass("color-change");
        $(".p-tag-exp").addClass("color-change-p");

        $('#phone-logo img').attr('src', 'img/phone-call.svg');

    }
    else {
        $(".exp-us").removeClass("color-change");
        $(".p-tag-exp").removeClass("color-change-p");
        $('#phone-logo img').attr('src', 'img/phonecall.svg');
    }
});
</script>

 <script> (function() { var qs,js,q,s,d=document, gi=d.getElementById, ce=d.createElement, gt=d.getElementsByTagName, id="typef_orm_share", b="https://embed.typeform.com/"; if(!gi.call(d,id)){ js=ce.call(d,"script"); js.id=id; js.src=b+"embed.js"; q=gt.call(d,"script")[0]; q.parentNode.insertBefore(js,q) } })() </script>
</body>

</html>
